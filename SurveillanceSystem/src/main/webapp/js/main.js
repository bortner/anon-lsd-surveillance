/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
/**
 * Creates an OpenLayers Surveillance Map. Allows to add n Layers.
 * 
 * @param {type} layers
 * @returns {ol.Map}
 */
function createSurveillanceMap() {
    var pinLayer = createPins();
    var geoLayer = createGeoLayer();
    var heatLayer = createHeatmap();
    var coordinate2count = {};
    var layers = [geoLayer, heatLayer, pinLayer];
    //pusher configuration
    //subscribe to the pusher channel by calling a 
    var channel = 'channel-one';
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SurveillanceSystem/services/surveillance/' + channel
    });

    //my pusher key - will be removed if the code will be public
    var pusher = new Pusher('6ab873bc7e5f554e857c');
    //"channel-one", "test_event"
    var channel = pusher.subscribe(channel);
    var idx = 0;
    channel.bind('release_cluster', function (data) {
        //update the views
        var cluster = data;
        for (var i in cluster) {
            var dp = cluster[i];
//            console.log(dp);

            //add a datapoint:
            var lat = dp.where.lat;
            var lng = dp.where.lng;

            idx++;
            var iconFeature = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.transform([lng, lat], 'EPSG:4326',
                        'EPSG:3857')),
                name: 'Marker ' + idx
            });
            
            var key = lat + lng;
            var old = coordinate2count[key];
            var count = 1;
            if (!isNaN(parseFloat(old))) {
                count = old + 1;
            }
            coordinate2count[key] = count;
            iconFeature.coloring = count;
            
            //add the temporary feature to the layer
            pinLayer.getSource().addFeature(iconFeature);
            geoLayer.getSource().addFeature(iconFeature);
            heatLayer.getSource().addFeature(iconFeature);
        }

    });

    //add all layers as overlay
    var overlays = new ol.layer.Group({
        title: 'Overlays',
        layers: [
        ]
    });
    for (var i in layers) {
        console.log('adding layer: ' + layers[i].title);
        overlays.getLayers().insertAt(i, layers[i]);
    }
    //create the map
    var map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Group({
                title: 'Maps',
                layers: [new ol.layer.Tile({
                        title: 'OSM',
                        type: 'base',
                        visible: true,
                        source: new ol.source.OSM()
                    })]
            }),
            overlays
        ],
        view: new ol.View({
            center: ol.proj.transform([15.9, 47.9], 'EPSG:4326', 'EPSG:3857'),
            zoom: 8
        })
    });

    //allow switching layers - https://github.com/walkermatt/ol3-layerswitcher
    var layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Legende' // Optional label for button
    });
    map.addControl(layerSwitcher);

    map.on('singleclick', function (evt) {
        displayFeatureInfo(map, evt.pixel);
    });

    return map;
}

