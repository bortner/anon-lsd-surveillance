/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
/**
 * Returns the GeoJSON Layer for the Surveillance System.
 * 
 * @returns {ol.layer.Vector}
 */
var nearestShape = {};
var featureCache = {};
function createGeoLayer() {
    //create a new layer
    var geoLayer = new ol.layer.Vector({
        title: 'GeoJson',
        source: new ol.source.GeoJSON({
            projection: 'EPSG:900913',
            url: 'resources/bezirke.json'
        }),
        style: (function () {
            return function (feature, resolution) {
                var text = resolution < 5000 ? feature.get('name') : '';
                var coordinates = feature.o.geometry.getCoordinates()[0];
                var coord;
//                console.log(text+" -> "+feature.coloring);
                if (coordinates.length > 1) {
                    //we have  a shape...
                    coord = getCenter(coordinates);
                    nearestShape[text] = coord;
                    featureCache[text] = feature;
                } else if (coordinates instanceof Array) {
                    console.log("Is an Array");
                    var array = coordinates[0];
                    coord = getCenter(array);
                    nearestShape[text] = coord;
                    featureCache[text] = feature;
                } else {
                    //multi shapes are also detected, they return one array containing arrays
                    coord = feature.o.geometry.getCoordinates();
                    var lbl = detectNearCoordinates(coord);
                    var cjdn = featureCache[lbl];
                    cjdn.coloring = feature.coloring;
                    feature = cjdn;
                    featureCache[lbl] = cjdn;
                }
                //add a style for each incidence rate
                var selector = feature.coloring;
                if (selector > 300) {
                    //red = high
                    return createStyle('#800026');
                } else if(selector > 100) {
                    //gray =  medium
                    return createStyle('#848484');;
                } else if(selector > 10) {
                    //blue = low
                    return createStyle('#0404B4');;
                } else {
                    //yellow = unset
                    return createStyle('#FFEDA0');;
                }
            };
        }())
    });
    return geoLayer;
}

/**
 * Creates a geo-shape style for the given color
 * @param {type} color
 * @returns {Array}
 */
function createStyle(color){
   return [new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: color
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#319FD3',
                        width: 1
                    }),
                    text: new ol.style.Text({
                        font: '12px Calibri,sans-serif',
                        fill: new ol.style.Fill({
                            color: '#000'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#fff',
                            width: 3
                        })})
                })];
}


// da kommt immer irgend eine shape heraus
function detectNearCoordinates(coordinate) {
    var minX = Number.POSITIVE_INFINITY;
    var minY = Number.POSITIVE_INFINITY;
    var lbl;
    for (var cachedObj in nearestShape) {
//        console.log(cachedObj);
        var shapeCoordinate = nearestShape[cachedObj];
//        console.log(shapeCoordinate[0]);
//        console.log(shapeCoordinate[1]);
        var curX = Math.abs(coordinate[0] - shapeCoordinate[0]);
        var curY = Math.abs(coordinate[1] - shapeCoordinate[1]);

        //liefer eisenstadt umgebung anstatt von wien umgebung
        var sum = curX + curY;
        var oldSum = minX + minY;
        if (oldSum > sum) {
            minX = curX;
            minY = curY;
            lbl = cachedObj;
        }
    }
    return lbl;
}


/**
 * calculates the centeroid of a given shape array. 
 * @param {type} coordinates
 * @returns {Number|String|getDimensionCenter.dimension}
 */
function getCenter(coordinates) {
    var center = {}
    var x = {};
    var y = {};
    for (var i = 0; i < coordinates.length; i++) {
        var coord = coordinates[i];
        x[i] = coord[0];
        y[i] = coord[1];
    }
    center[0] = getDimensionCenter(x, coordinates.length);
    center[1] = getDimensionCenter(y, coordinates.length);

    return center;
}

/**
 * Helper Function for {getCenter}.
 * @param {type} dimension
 * @param {type} size
 * @returns {Number|String|getDimensionCenter.dimension}
 */
function getDimensionCenter(dimension, size) {
    var centeroid;
    var lowest = Number.POSITIVE_INFINITY;
    var highest = Number.NEGATIVE_INFINITY;
    var tmp;
    for (var i = 0; i < size; i++) {
        tmp = dimension[i];
        if (tmp < lowest)
            lowest = tmp;
        if (tmp > highest)
            highest = tmp;
    }
    centeroid = (highest - lowest) / 2 + lowest;
    return centeroid;
}