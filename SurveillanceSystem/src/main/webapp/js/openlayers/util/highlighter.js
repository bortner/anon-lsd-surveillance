/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/


// caches the actual selected feature
function createFeatureOverlay(map) {
    var highlightStyleCache = {};

    /**
     * define a new Overlay 
     * 
     * @type ol.FeatureOverlay
     */
    var featureOverlay = new ol.FeatureOverlay({
        map: map,
        style: function (feature, resolution) {
            var text = resolution < 5000 ? feature.get('name') : '';
            if (!highlightStyleCache[text]) {
                highlightStyleCache[text] = [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: '#f00',
                            width: 1
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(255,0,0)',
                            opacity: 0.4
                        }),
                        text: new ol.style.Text({
                            font: '12px Calibri,sans-serif',
                            text: text,
                            fill: new ol.style.Fill({
                                color: '#000'
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#f00',
                                width: 3
                            })
                        })
                    })];
            }
            return highlightStyleCache[text];
        }
    });
    return featureOverlay;
}
//the actual selected feature
var highlight;
var featureOverlay;
var dialog;
/**
 * 
 * @param {type} pixel
 * @returns {undefined}
 */
var displayFeatureInfo = function (map, pixel) {

    var feature = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
        return feature;
    });
    if (typeof featureOverlay === 'undefined') {
        featureOverlay = createFeatureOverlay(map);
    }
    if (feature !== highlight) {
        if (highlight) {
            featureOverlay.removeFeature(highlight);
            dialog.dialog("destroy").remove();
        }
        if (feature) {
            featureOverlay.addFeature(feature);
        }
        highlight = feature;
    }
    
    //build the pop-up menu 
    var name = highlight.get('name');
    var pixCoord = map.getCoordinateFromPixel(pixel);
    var coord = ol.proj.transform(pixCoord, 'EPSG:3857', 'EPSG:4326');
    var output = 'clicked on:<br/>Longitude ' + coord[0] + '<br/>Latitude: ' + coord[1]+'<br/>';
    output = output+ '<div id="container" style="height: 400px; min-width: 310px"></div>';

    createJQueryDialog(name, output);

};

function createJQueryDialog(name, output) {
    //jquery dialog
    dialog = $('<div title=" ' + name + '! ">' + output + '</div>');
    dialog.dialog({
        height: 500,
        width: 500,
        show: {
            effect: "fade",
            duration: 1000
        },
        hide: {
            effect: "fade",
            duration: 500
        }
    });


    $(function () {

        $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function (data) {
            // Create the chart
            $('#container').highcharts('StockChart', {
                rangeSelector: {
                    selected: 1
                },
                title: {
                    text: 'Information Loss'
                },
                series: [{
                        name: 'ILM',
                        data: data,
                        tooltip: {
                            valueDecimals: 2
                        }
                    }]
            });
        });

    });

}


