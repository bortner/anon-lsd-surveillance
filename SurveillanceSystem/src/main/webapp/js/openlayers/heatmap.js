/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
function createHeatmap() {

    var data = new ol.source.Vector();

    // created for owl range of data
    var coord = ol.proj.transform([16.755867, 48.269412], 'EPSG:4326', 'EPSG:3857');

    var lonLat = new ol.geom.Point(coord);

    var pointFeature = new ol.Feature({
        name: 'example Feature',
        geometry: lonLat,
        weight: 20 // e.g. temperature
    });

    data.addFeature(pointFeature);

    var heatmap = new ol.layer.Heatmap({
        title: 'HeatMap',
        source: data,
        radius: 20
    });

//    heatmap.getSource().on('addfeature', function (event) {
//        // 2012_Earthquakes_Mag5.kml stores the magnitude of each earthquake in a
//        // standards-violating <magnitude> tag in each Placemark.  We extract it from
//        // the Placemark's name instead.
//        var name = event.feature.get('name');
//        var magnitude = parseFloat(name.substr(2));
//        event.feature.set('weight', magnitude - 5);
//    });
    heatmap.setVisible(false);
    return heatmap;
}
