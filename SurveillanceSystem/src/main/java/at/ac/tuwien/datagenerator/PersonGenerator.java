/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.datagenerator;

import at.ac.tuwien.datagenerator.ontology.ClinicalCondition;
import at.ac.tuwien.datagenerator.ontology.Person;
import at.ac.tuwien.datagenerator.ontology.Point;
import at.ac.tuwien.datagenerator.ontology.sickness.Influenza;
import at.ac.tuwien.datagenerator.util.ValueComparator;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.ibm.icu.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.time.DateUtils;
import org.geojson.LngLatAlt;
import org.ontoware.rdf2go.impl.jena26.ModelImplJena26;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.node.DatatypeLiteral;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class generates a Single Person, that is fed into CQELS. The output is a
 * simple {@link com.hp.hpl.jena.rdf.model.Model}
 *
 * The geo-Model is generated in {@link GenerateGeoData}
 *
 * @see http://www.geotools.org/
 * @see GenerateGeoData
 * @author Bernhard
 */
public class PersonGenerator {

    Map<String, Double> probability = null;

    static final URI DOUBLE_DATATYPE = new URIImpl(XSDDatatype.XSDdouble.getURI());
    static final URI STRING_DATATYPE = new URIImpl(XSDDatatype.XSDstring.getURI());
    static final URI DATE_DATATYPE = new URIImpl(XSDDatatype.XSDdate.getURI());
    
    private final static Logger logger = LoggerFactory.getLogger(PersonGenerator.class);

    static final String BASE_URL = "http://ldlab.tuwien.ac.at/";

    private static long cnt = 1;
    /**
     * The encapsulated {@link org.ontoware.rdf2go.model.Model}.
     */
    private final Model model;

    public PersonGenerator(com.hp.hpl.jena.rdf.model.Model model) {
        this.model = new ModelImplJena26(model);
        ValueComparator bvc = new ValueComparator(GeoDataCache.PROBABILITY);
        this.probability = new TreeMap<>(bvc);
        probability.putAll(GeoDataCache.PROBABILITY);

    }

    /**
     * Generates a person
     *
     * @return
     */
    public Person generatePerson() {
        model.open();
        Person p = new Person(model, BASE_URL + "Person"+cnt, true);
        p.addPersonSocialID(123456789);
        DatatypeLiteral familyName = model.createDatatypeLiteral("Ortner", STRING_DATATYPE);
        p.addPersonFamilyName(familyName);
        DatatypeLiteral firstName = model.createDatatypeLiteral("Bernhard", STRING_DATATYPE);
        p.addPersonFirstName(firstName);
        
         int day = (int) (Math.random() * 365);
        int year = (int) (Math.random() * 50);
        int month = (int) (Math.random() * 12);
        Date d = DateUtils.addDays(new Date(), -day);
        d = DateUtils.addYears(d, -year);
        d = DateUtils.addMonths(d, -month);
        
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(d);
        DatatypeLiteral bDay = model.createDatatypeLiteral(date, DATE_DATATYPE);
        p.addPersonBirthday(bDay);
        DatatypeLiteral sex = model.createDatatypeLiteral("male", STRING_DATATYPE);
        p.addPersonGender(sex);

        //the person suffers from influenza !
        ClinicalCondition condition = new ClinicalCondition(model, BASE_URL + "Condition"+cnt, true);
        condition.addValue(new Influenza(model, BASE_URL + "Influenza", true));
        
        String startDate = sdf.format(new Date());
        DatatypeLiteral sDay = model.createDatatypeLiteral(startDate, DATE_DATATYPE);
        condition.setSufferingStartDate(sDay);
        p.addPersonSuffers(condition);

        //Where Dimension
        LngLatAlt coordinate = selectShape();
        Point point = new Point(model, BASE_URL + "Place"+cnt, true);
        Double lat = coordinate.getLatitude();
        DatatypeLiteral latLiteral = model.createDatatypeLiteral(lat.toString(), DOUBLE_DATATYPE);
        Double lng = coordinate.getLongitude();
        DatatypeLiteral lngLiteral = model.createDatatypeLiteral(lng.toString(), DOUBLE_DATATYPE);
        point.addPersonLatitude(latLiteral);
        point.addPersonLongitude(lngLiteral);
        p.addPersonLocation(point);
        cnt++;
        return p;
    }

    /**
     * Select a given shape.
     */
    public LngLatAlt selectShape() {
        //this is the algorithm that chooses a shape
        double rnd = Math.random();
        String selectedShape = null;
        for (Map.Entry<String, Double> entry : probability.entrySet()) {
            String name = entry.getKey();
            Double prob = entry.getValue();
            if (rnd > prob) {
                selectedShape = name;
                break;
            }
        }

        logger.trace("Selected Shape: " + selectedShape);
        return GeoDataCache.COORDINATE.get(selectedShape);
    }
}
