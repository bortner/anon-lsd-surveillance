/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.datagenerator;

/**
 * This is the datagenerator for the engine. As use case is influenca used. The
 * model is theoretically defined by Fricker - Appendix B. Used formula for
 * modelling an outbreak: UTM1 - B.3 with Seasonal effects The model paremeter
 * are taken from AGES, MedUni Wien, Statistik Austria or DINÖ. The model is
 * limited to 100k citizen.
 *
 *
 * @see <a href="http://www.influenza.at/">Diagnostic Influenca Network
 * Austria</a>
 * @see
 * <a href="http://www.virologie.meduniwien.ac.at/home/upload/vei/2014/0614.pdf">Influenca
 * Report - MedUni Wien</a>
 * @see
 * <a href="http://www.ages.at/ages/gesundheit/mensch/influenza/aktuelle-influenzameldungen/">Influenca
 * AGES</a>
 * @see
 * <a href="http://www.statistik.at/web_de/statistiken/gesundheit/gesundheitsversorgung/personal_im_gesundheitswesen/022350.html">Statistik
 * Austria</a>
 *
 * Markus Hudec - Uni Wien - Statistik
 * 
 * @author Bernhard
 */
public class DataGenerator {

    /**
     * Counted general practitioners in austria and 100k citizen
     *
     * @see
     * <a href="http://www.statistik.at/web_de/statistiken/gesundheit/gesundheitsversorgung/personal_im_gesundheitswesen/022350.html">Statistik
     * Austria</a>
     */
    private static final double countGPs = 163.7;

    /**
     * Maximum amount of reported influenca cases in austria (per Week) and 100k
     * citizen.
     *
     * @see
     * <a href="http://www.ages.at/ages/gesundheit/mensch/influenza/aktuelle-influenzameldungen/">Influenza
     * AGES</a>
     */
    private static final int maxCases = 1100;

    /**
     * Duration of influenca of the season 2013 / 14
     *
     * @see
     * <a href="http://www.influenza.at/saison-20132014">Duration of
     * influenca</a>
     */
    private static long influencaDuration = 56;

    /**
     * Population of Austria in 100k citizen.
     */
    private int population = 83;

    /**
     * From Fricker Page 364 - Example B.4
     */
    private double A = 6.4;

    /**
     * From Fricker Page 364 - Example B.4
     */
    private double B = 3;

    /**
     * Standard deviation. From Fricker Page 364 - Example B.4
     */
    private double sigma = 5;

    /**
     * mean. From Fricker Page 364 - Example B.4
     */
    private double mu = 16.75;

    /**
     * Start Day.
     */
    private double tau = 0;

    /**
     * Constructor.
     */
    public DataGenerator(double startDay, int scale) {
        this.tau = startDay;
        this.population = scale;
    }

    /**
     * get the maximum sick persons.
     *
     * @param time
     * @return
     */
    public double getData(long time) {
        double y = 0;
        double val = mu + seasonalDeviation(time)
                + withinSeasonalDeviation(time) + dayOfEffect(time) + outbreakLevel(time) + noise();
        y = Math.max(0, Math.floor(val));
        y = population * y;
        return y;
    }

    /**
     * Gamma Function:
     *
     * Gamma_t = A * (sin(2*pi*i)/ 365)
     *
     * i = t
     *
     * @param time
     */
    public double seasonalDeviation(long time) {
        double gamma;
        double numerator = 2 * Math.PI * time;
        double denominator = 365;
        gamma = A * (Math.sin((numerator / denominator)));
        return gamma;
    }

    /**
     * Theta Function:
     *
     * Theta_t = B * (sin(2*pi*(t + tau) / 91.25))
     *
     * @param time
     */
    public double withinSeasonalDeviation(long time) {
        double theta;
        double numerator = 2 * Math.PI * (time + this.tau);
        double denominator = 91.25;
        theta = B * (Math.sin((numerator / denominator)));
        return theta;
    }

    /**
     * Delta Function (Average Absolute Deviation): n = 7 because a week has 7
     * days.
     *
     * @return
     */
    public double dayOfEffect(long time) {
        double delta = 0;
        delta = delta + (1 / 7) * Math.pow((seasonalDeviation(time) - this.mu), 2);
        return delta;
    }

    /**
     * o Function (signum Function)
     *
     * o = Magnitude x I{tau < t < tau+duration} day: 83 - 139
     *
     * @return
     */
    private double outbreakLevel(long time) {
        long startDay = (long) this.tau;
        long endDay = (startDay + this.influencaDuration);
        double o = 0;
        if ((startDay <= time) && (time <= endDay)) {
            //start the outbreak
            o = 7 * Math.signum(A);
        }
        return o;
    }

    /**
     * Random Noise, generated by Colt
     *
     * @return
     */
    private double noise() {
        return Math.random() * (sigma * sigma);
    }
}
