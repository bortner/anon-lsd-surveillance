/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.datagenerator;

import java.util.HashMap;
import java.util.Map;
import org.geojson.LngLatAlt;

/**
 * The Cached GeoDataCache results from {@link GenerateGeoData}
 *
 * {@link #PROBABILITY} contains the result from {@link GenerateGeoData#calcProbOfShapes() }
 * {@link #COORDINATE} contains the result from {@link GenerateGeoData#getShapes()
 * }
 *
 * @author Bernhard
 */
public class GeoDataCache {

    public static Map<String, Double> PROBABILITY = new HashMap<>();

    public static Map<String, LngLatAlt> COORDINATE = new HashMap<>();

    /**
     * Feedback Hudec:
     * eventuell hier diesen wert durch eine rel. probability ersetzen und dann 
     * damit nachdem der shape gezogen worden ist wieder gewichten.
     */
    static {
        //initalize the probability of all shapes
        PROBABILITY.put("Wien", 0.20916850963473715);
        PROBABILITY.put("Graz (Stadt)", 0.0319654721707875);
        PROBABILITY.put("Linz (Stadt)", 0.02294601800504823);
        PROBABILITY.put("Innsbruck-Land", 0.020088746608070543);
        PROBABILITY.put("Salzburg (Stadt)", 0.017359930480245117);
        PROBABILITY.put("Salzburg-Umgebung", 0.01719939099179307);
        PROBABILITY.put("Graz-Umgebung", 0.017118766071707637);
        PROBABILITY.put("Linz-Land", 0.01675719704683112);
        PROBABILITY.put("Baden", 0.016584108011346684);
        PROBABILITY.put("Voecklabruck", 0.015568186661488992);
        PROBABILITY.put("Bregenz", 0.015221416630754441);
        PROBABILITY.put("Innsbruck-Stadt", 0.014749151129696016);
        PROBABILITY.put("Wien-Umgebung", 0.01389246695680588);
        PROBABILITY.put("Moedling", 0.013695225962881755);
        PROBABILITY.put("Amstetten", 0.013371660754961807);
        PROBABILITY.put("Kufstein", 0.012231901422124141);
        PROBABILITY.put("Feldkirch", 0.012016428067417114);
        PROBABILITY.put("Bruck-Muerzzuschlag", 0.011940420433503976);
        PROBABILITY.put("Gmunden", 0.011784735015130492);
        PROBABILITY.put("Braunau am Inn", 0.011702097431841753);
        PROBABILITY.put("Gaenserndorf", 0.011538479752608174);
        PROBABILITY.put("Sankt Poelten (Land)", 0.01152723251706028);
        PROBABILITY.put("Klagenfurt Stadt", 0.011441398351036877);
        PROBABILITY.put("Hartberg-Fuerstenfeld", 0.010646869953542998);
        PROBABILITY.put("Weiz", 0.010459218707822867);
        PROBABILITY.put("Suedoststeiermark", 0.010172355005374995);
        PROBABILITY.put("Neunkirchen", 0.010127129279277147);
        PROBABILITY.put("Zell am See", 0.010059053906224101);
        PROBABILITY.put("Dornbirn", 0.009958775921918139);
        PROBABILITY.put("Urfahr-Umgebung", 0.009721044880021595);
        PROBABILITY.put("Schwaz", 0.00950746579656474);
        PROBABILITY.put("Liezen", 0.009426722484526171);
        PROBABILITY.put("Sankt Johann im Pongau", 0.009307265003812222);
        PROBABILITY.put("Spittal an der Drau", 0.009112747024810217);
        PROBABILITY.put("Korneuburg", 0.009041593460975644);
        PROBABILITY.put("Melk", 0.009041475069022508);
        PROBABILITY.put("Leibnitz", 0.008971150248859885);
        PROBABILITY.put("Wiener Neustadt (Land)", 0.008913138191823378);
        PROBABILITY.put("Mistelbach", 0.008778763325014325);
        PROBABILITY.put("Murtal", 0.008647466648986801);
        PROBABILITY.put("Tulln", 0.008536533388898624);
        PROBABILITY.put("Wels-Land", 0.008121687985111028);
        PROBABILITY.put("Perg", 0.007845716342351643);
        PROBABILITY.put("Freistadt", 0.007720102480074634);
        PROBABILITY.put("Villach-Land", 0.007608814044127049);
        PROBABILITY.put("Grieskirchen", 0.007451352746456529);
        PROBABILITY.put("Kitzbuehel", 0.007377949735512377);
        PROBABILITY.put("Bludenz", 0.007233748336593058);
        PROBABILITY.put("Leoben", 0.007226763211358051);
        PROBABILITY.put("Deutschlandsberg", 0.007158687838305006);
        PROBABILITY.put("Villach Stadt", 0.007103990755956299);
        PROBABILITY.put("Wels (Stadt)", 0.007025260107121039);
        PROBABILITY.put("Ried im Innkreis", 0.006951265136411209);
        PROBABILITY.put("Steyr-Land", 0.006939899508910178);
        PROBABILITY.put("Klagenfurt Land", 0.00691823378148634);
        PROBABILITY.put("Hallein", 0.006906512978125902);
        PROBABILITY.put("Imst", 0.006780425548036351);
        PROBABILITY.put("Neusiedl am See", 0.006689618919981247);
        PROBABILITY.put("Rohrbach", 0.006683817714277596);
        PROBABILITY.put("Schaerding", 0.0066639278661507935);
        PROBABILITY.put("Kirchdorf an der Krems", 0.0065791592277056115);
        PROBABILITY.put("Sankt Veit an der Glan", 0.0065582038520005876);
        PROBABILITY.put("Oberwart", 0.006342612105340424);
        PROBABILITY.put("Wolfsberg", 0.006330654518073716);
        PROBABILITY.put("St.Poelten (Stadt)", 0.006173548396262603);
        PROBABILITY.put("Voitsberg", 0.006108906389850495);
        PROBABILITY.put("Hollabrunn", 0.005927293133740286);
        PROBABILITY.put("Lienz", 0.005800021784119377);
        PROBABILITY.put("Landeck", 0.005198117094377329);
        PROBABILITY.put("Bruck an der Leitha", 0.005163665036014832);
        PROBABILITY.put("Zwettl", 0.005102929964056203);
        PROBABILITY.put("Wiener Neustadt (Stadt)", 0.005004783034906683);
        PROBABILITY.put("Voelkermarkt", 0.004980512684513859);
        PROBABILITY.put("Eisenstadt-Umgebung", 0.0049101878643512354);
        PROBABILITY.put("Scheibbs", 0.004862712691143808);
        PROBABILITY.put("Mattersburg", 0.0046331506940136295);
        PROBABILITY.put("Steyr (Stadt)", 0.004513101253534);
        PROBABILITY.put("Oberpullendorf", 0.004443723568996462);
        PROBABILITY.put("Gmuend", 0.004430226886338989);
        PROBABILITY.put("Eferding", 0.0037839252141710434);
        PROBABILITY.put("Reutte (Ausserfern)", 0.0037497099397148176);
        PROBABILITY.put("Horn", 0.0037024715504136614);
        PROBABILITY.put("Feldkirchen", 0.003561466734229008);
        PROBABILITY.put("Murau", 0.0034025847331208593);
        PROBABILITY.put("Waidhofen an der Thaya", 0.0031283889696585103);
        PROBABILITY.put("Guessing", 0.003124837211064438);
        PROBABILITY.put("Lilienfeld", 0.0030829264596543902);
        PROBABILITY.put("Krems an der Donau (Stadt)", 0.0028514701912740396);
        PROBABILITY.put("Tamsweg", 0.0024211154416256637);
        PROBABILITY.put("Hermagor", 0.002195815554808371);
        PROBABILITY.put("Jennersdorf", 0.002057178577686432);
        PROBABILITY.put("Eisenstadt (Stadt)", 0.0015965154880353092);
        PROBABILITY.put("Waidhofen an der Ybbs (Stadt)", 0.0013426831405123057);
        PROBABILITY.put("Rust (Stadt)", 0.0);

        //initialize the coordinate LngLatAlt(double longitude, double latitude)
        COORDINATE.put("Eisenstadt (Stadt)", new LngLatAlt(16.534518499999997, 47.8389925));
        COORDINATE.put("Rust (Stadt)", new LngLatAlt(16.6867335, 47.803743999999995));
        COORDINATE.put("Wiener Neustadt (Stadt)", new LngLatAlt(16.216001, 47.80744095));
        COORDINATE.put("Waidhofen an der Ybbs (Stadt)", new LngLatAlt(14.79115795, 47.95128285));
        COORDINATE.put("Innsbruck-Stadt", new LngLatAlt(11.378718500000002, 47.2854521));
        COORDINATE.put("Wels-Land", new LngLatAlt(13.96262785, 48.1289285));
        COORDINATE.put("Gmunden", new LngLatAlt(13.766948450000001, 47.7583595));
        COORDINATE.put("Eisenstadt-Umgebung", new LngLatAlt(16.5762265, 47.8539049));
        COORDINATE.put("Mattersburg", new LngLatAlt(16.4104405, 47.728027));
        COORDINATE.put("Hermagor", new LngLatAlt(13.13341145, 46.6534635));
        COORDINATE.put("Wels (Stadt)", new LngLatAlt(14.027254, 48.1684425));
        COORDINATE.put("Waidhofen an der Thaya", new LngLatAlt(15.36320325, 48.8475516));
        COORDINATE.put("Grieskirchen", new LngLatAlt(13.783386449999998, 48.2797595));
        COORDINATE.put("Gmuend", new LngLatAlt(14.938528600000001, 48.78603655));
        COORDINATE.put("Freistadt", new LngLatAlt(14.65281665, 48.4732361));
        COORDINATE.put("Villach Stadt", new LngLatAlt(13.8368371, 46.61350935));
        COORDINATE.put("Kirchdorf an der Krems", new LngLatAlt(14.22122865, 47.853954));
        COORDINATE.put("St.Poelten (Stadt)", new LngLatAlt(15.647532, 48.193415));
        COORDINATE.put("Linz-Land", new LngLatAlt(14.3061595, 48.2007275));
        COORDINATE.put("Steyr (Stadt)", new LngLatAlt(14.423226, 48.055846450000004));
        COORDINATE.put("Leibnitz", new LngLatAlt(15.499215, 46.816223449999995));
        COORDINATE.put("Scheibbs", new LngLatAlt(15.0894653, 47.9270825));
        COORDINATE.put("Horn", new LngLatAlt(15.68304955, 48.72986775));
        COORDINATE.put("Krems an der Donau (Stadt)", new LngLatAlt(15.62662325, 48.409683650000005));
        COORDINATE.put("Suedoststeiermark", new LngLatAlt(15.847461, 46.8596175));
        COORDINATE.put("Melk", new LngLatAlt(15.237461249999999, 48.19209275));
        COORDINATE.put("Sankt Poelten (Land)", new LngLatAlt(15.638213, 48.138728));
        COORDINATE.put("Lilienfeld", new LngLatAlt(15.5554726, 47.917688999999996));
        COORDINATE.put("Deutschlandsberg", new LngLatAlt(15.20349925, 46.80610075));
        COORDINATE.put("Zwettl", new LngLatAlt(15.162521850000001, 48.55335055));
        COORDINATE.put("Korneuburg", new LngLatAlt(16.22570135, 48.440485100000004));
        COORDINATE.put("Dornbirn", new LngLatAlt(9.74637665, 47.373649549999996));
        COORDINATE.put("Salzburg (Stadt)", new LngLatAlt(13.05655345, 47.803551));
        COORDINATE.put("Liezen", new LngLatAlt(14.351213, 47.5072505));
        COORDINATE.put("Urfahr-Umgebung", new LngLatAlt(14.25293325, 48.4220452));
        COORDINATE.put("Linz (Stadt)", new LngLatAlt(14.32749055, 48.2950354));
        COORDINATE.put("Gaenserndorf", new LngLatAlt(16.739467400000002, 48.379936099999995));
        COORDINATE.put("Oberwart", new LngLatAlt(16.252191, 47.2874425));
        COORDINATE.put("Steyr-Land", new LngLatAlt(14.461062949999999, 47.907030500000005));
        COORDINATE.put("Hollabrunn", new LngLatAlt(16.018573500000002, 48.67025005));
        COORDINATE.put("Hartberg-Fuerstenfeld", new LngLatAlt(15.94109355, 47.24802835));
        COORDINATE.put("Jennersdorf", new LngLatAlt(16.143137, 46.98185475));
        COORDINATE.put("Krems (Land)", new LngLatAlt(15.52813265, 48.45101185));
        COORDINATE.put("Mistelbach", new LngLatAlt(16.56388595, 48.5644834));
        COORDINATE.put("Feldkirchen", new LngLatAlt(14.025209499999999, 46.79614535));
        COORDINATE.put("Tamsweg", new LngLatAlt(13.65696195, 47.12002645));
        COORDINATE.put("Voecklabruck", new LngLatAlt(13.5529268, 47.9470687));
        COORDINATE.put("Amstetten", new LngLatAlt(14.733954, 48.08960715));
        COORDINATE.put("Perg", new LngLatAlt(14.695346650000001, 48.290426));
        COORDINATE.put("Guessing", new LngLatAlt(16.304411, 47.1139369));
        COORDINATE.put("Landeck", new LngLatAlt(10.4600647, 47.03668665));
        COORDINATE.put("Weiz", new LngLatAlt(15.66657775, 47.2951665));
        COORDINATE.put("Klagenfurt Stadt", new LngLatAlt(14.313865, 46.641492150000005));
        COORDINATE.put("Graz (Stadt)", new LngLatAlt(15.44195735, 47.073193200000006));
        COORDINATE.put("Graz-Umgebung", new LngLatAlt(15.3895675, 47.1129655));
        COORDINATE.put("Feldkirch", new LngLatAlt(9.68972675, 47.25743105));
        COORDINATE.put("Baden", new LngLatAlt(16.213391, 48.010474349999996));
        COORDINATE.put("Sankt Johann im Pongau", new LngLatAlt(13.30415715, 47.28461695));
        COORDINATE.put("Bludenz", new LngLatAlt(9.914628700000002, 47.067888350000004));
        COORDINATE.put("Wien", new LngLatAlt(16.37967255, 48.220287400000004));
        COORDINATE.put("Moedling", new LngLatAlt(16.228883500000002, 48.0870161));
        COORDINATE.put("Tulln", new LngLatAlt(15.991430999999999, 48.3479375));
        COORDINATE.put("Wien-Umgebung", new LngLatAlt(16.5219927, 48.07244515));
        COORDINATE.put("Lienz", new LngLatAlt(12.54229005, 46.90651475));
        COORDINATE.put("Hallein", new LngLatAlt(13.27695675, 47.612685));
        COORDINATE.put("Murau", new LngLatAlt(14.1867095, 47.119378499999996));
        COORDINATE.put("Kitzbuehel", new LngLatAlt(12.351709, 47.48256));
        COORDINATE.put("Kufstein", new LngLatAlt(12.05477975, 47.532165750000004));
        COORDINATE.put("Zell am See", new LngLatAlt(12.57649915, 47.35900785));
        COORDINATE.put("Salzburg-Umgebung", new LngLatAlt(13.205458, 47.832446000000004));
        COORDINATE.put("Neunkirchen", new LngLatAlt(15.910100450000002, 47.6641404));
        COORDINATE.put("Wiener Neustadt (Land)", new LngLatAlt(16.0072675, 47.8656925));
        COORDINATE.put("Bruck-Muerzzuschlag", new LngLatAlt(15.3969155, 47.567826499999995));
        COORDINATE.put("Leoben", new LngLatAlt(14.916744999999999, 47.430212));
        COORDINATE.put("Bruck an der Leitha", new LngLatAlt(16.787043500000003, 48.031641500000006));
        COORDINATE.put("Neusiedl am See", new LngLatAlt(16.92818055, 47.89955185));
        COORDINATE.put("Oberpullendorf", new LngLatAlt(16.49812955, 47.5050867));
        COORDINATE.put("Klagenfurt Land", new LngLatAlt(14.2956758, 46.582091750000004));
        COORDINATE.put("Villach-Land", new LngLatAlt(13.7664898, 46.636290349999996));
        COORDINATE.put("Wolfsberg", new LngLatAlt(14.840191, 46.82549395));
        COORDINATE.put("Sankt Veit an der Glan", new LngLatAlt(14.3140684, 46.881983500000004));
        COORDINATE.put("Murtal", new LngLatAlt(14.6381885, 47.23282185));
        COORDINATE.put("Voitsberg", new LngLatAlt(15.07696, 47.0614925));
        COORDINATE.put("Spittal an der Drau", new LngLatAlt(13.250970899999999, 46.893028400000006));
        COORDINATE.put("Bregenz", new LngLatAlt(9.899203400000001, 47.41114935));
        COORDINATE.put("Voelkermarkt", new LngLatAlt(14.6928763, 46.5991726));
        COORDINATE.put("Reutte (Ausserfern)", new LngLatAlt(10.58317355, 47.36101315));
        COORDINATE.put("Innsbruck-Land", new LngLatAlt(11.34741545, 47.20612895));
        COORDINATE.put("Imst", new LngLatAlt(10.8530882, 47.0797467));
        COORDINATE.put("Schaerding", new LngLatAlt(13.636969149999999, 48.4182452));
        COORDINATE.put("Braunau am Inn", new LngLatAlt(13.05449, 48.132659950000004));
        COORDINATE.put("Ried im Innkreis", new LngLatAlt(13.4119806, 48.212044899999995));
        COORDINATE.put("Schwaz", new LngLatAlt(11.759917999999999, 47.2817231));
        COORDINATE.put("Rohrbach", new LngLatAlt(13.97454355, 48.573092));
        COORDINATE.put("Eferding", new LngLatAlt(14.008572000000001, 48.3447225));
    }
}
