/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.datagenerator;

import at.ac.tuwien.datagenerator.util.ValueComparator;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.LngLatAlt;
import org.geojson.MultiPolygon;
import org.geojson.Polygon;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.opengis.feature.simple.SimpleFeatureType;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.geometry.BoundingBox;

import org.geotools.referencing.crs.DefaultGeographicCRS;

/**
 * This is the generator for selected Geo Shapes. It consists of two phases.
 * <p>
 * Phase 1 - generate a <Name, (Lat,Lng)> mapping from geoJSon Files:<br>
 * Input: the bezirke.json file as input data and generates a Pin from it.
 * Basically it tries to center the pin in the geoshape.
 * <p>
 * Phase 2 - calculate the choosing probability from the resulting Mapping. <br>
 * Sort the probability descending and use Math.random(). The first shape that
 * is lower than the rolled number will be used.
 * <p>
 * Both steps are precomputed in {@link GeoDataCache} class to improve performance.
 * The user can directly generate code from it by uncommenting appropriate
 * <code>System.out.println</code>
 *
 * @see GeoDataCache
 * @author Bernhard
 */
public class GenerateGeoData {

    static Map<String, Double> population = new HashMap<>();

    public static void main(String[] args) {
//        getShapes();
//        calcProbOfShapes();
        for (int i = 0; i < 10; i++) {
            selectShape();
        }
    }

    static void selectShape() {
        //this is the algorithm that chooses a shape
        ValueComparator bvc = new ValueComparator(GeoDataCache.PROBABILITY);
        Map<String, Double> probability = new TreeMap<>(bvc);
        probability.putAll(GeoDataCache.PROBABILITY);
        double rnd = Math.random();
        String selectedShape = null;
        for (Map.Entry<String, Double> entry : probability.entrySet()) {
            String name = entry.getKey();
            Double prob = entry.getValue();
            if (rnd > prob) {
                selectedShape = name;
                break;
            }
        }

        System.out.println("Probability: " + rnd);
        System.out.println("Selected Shape: " + selectedShape);
        System.out.println("Coordinates: " + GeoDataCache.COORDINATE.get(selectedShape));
    }

    /**
     * Calculates the probability that a certain shape is selected. Based on the
     * population of each shape. It has to fit the {@link #getShapes() } Naming
     * convention. Wikipedia uses slightly different naming conventions than the
     * official ones. Naturally this would be gathered from db pedia.
     */
    static void calcProbOfShapes() {

        ValueComparator bvc = new ValueComparator(population);
        Map<String, Double> probability = new TreeMap<>(bvc);
        Collection<Double> values = population.values();
        //sum will be precomputed
        Double sum = 0.0;
        for (Double o : values) {
            sum = sum + o;
        }
        for (Map.Entry<String, Double> entry : population.entrySet()) {
            String name = entry.getKey();
            Double pop = entry.getValue();
            Double prop = pop / sum;
            probability.put(name, prop);
        }
        System.out.println("Sorted probability: " + probability);
    }

    /**
     * Gets a shape from a geoJsonFile.
     */
    static void getShapes() {
        FileInputStream fir = null;
        try {
            System.out.println("Drawing Shape");
            //get the geo Shape file
            fir = new FileInputStream(new File("src/main/resources/bezirke.json"));
            FeatureCollection featureCollection
                    = new ObjectMapper().readValue(fir, FeatureCollection.class);
            List<Feature> features = featureCollection.getFeatures();
            //create the builder
            org.geotools.feature.simple.SimpleFeatureTypeBuilder builder = new org.geotools.feature.simple.SimpleFeatureTypeBuilder();
            // set the name
            builder.setName("MyFeatureType");
            //add a geometry property
            builder.setCRS(DefaultGeographicCRS.WGS84); // set crs first
            builder.add("location", Point.class); // then add geometry

            //build the type
            final SimpleFeatureType TYPE = builder.buildFeatureType();
            //create the builder
            SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(TYPE);
            GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
            for (Feature feat : features) {
                DefaultFeatureCollection col = new DefaultFeatureCollection("internal", TYPE);
                String name = feat.getProperty("name");
                if (feat.getGeometry() instanceof Polygon) {
                    Polygon polygon = (Polygon) feat.getGeometry();
                    int count = 1;
                    List<List<LngLatAlt>> coordinates = polygon.getCoordinates();
                    for (List<LngLatAlt> firstCoordinate : coordinates) {
                        for (LngLatAlt coord : firstCoordinate) {
                            double lat = coord.getLatitude();
                            double lon = coord.getLongitude();
                            Point point = geometryFactory.createPoint(new Coordinate(lat, lon));
                            featureBuilder.add(point);
                            SimpleFeature feature = featureBuilder.buildFeature(name + count);
                            col.add(feature);
                            count++;
                        }
                    }
                    //Add feature 1
                    //the bounding box for this shape
                    BoundingBox bounds = col.getBounds();
                    double maxLat = bounds.getMaxX();
                    double minLat = bounds.getMinX();
                    double avgLat = minLat + (maxLat - minLat) / 2;
                    double maxLon = bounds.getMaxY();
                    double minLon = bounds.getMinY();
                    double avgLon = minLon + (maxLon - minLon) / 2;
                    System.out.println("Generic Point on (X,Y): " + avgLat + " , " + avgLon);

                } else {
                    MultiPolygon multipolygon = (MultiPolygon) feat.getGeometry();
                    List<List<List<LngLatAlt>>> shapes = multipolygon.getCoordinates();
                    for (List<List<LngLatAlt>> coordinates : shapes) {
                        int count = 1;
                        for (List<LngLatAlt> firstCoordinate : coordinates) {
                            for (LngLatAlt coord : firstCoordinate) {
                                double lat = coord.getLatitude();
                                double lon = coord.getLongitude();
                                Point point = geometryFactory.createPoint(new Coordinate(lat, lon));
                                featureBuilder.add(point);
                                SimpleFeature feature = featureBuilder.buildFeature(name + count);
                                col.add(feature);
                                count++;
                            }
                        }
                        //Add feature 1
                        //the bounding box for this shape
                        BoundingBox bounds = col.getBounds();
                        double maxLat = bounds.getMaxX();
                        double minLat = bounds.getMinX();
                        double avgLat = minLat + (maxLat - minLat) / 2;
                        double maxLon = bounds.getMaxY();
                        double minLon = bounds.getMinY();
                        double avgLon = minLon + (maxLon - minLon) / 2;
                        System.out.println("Generic Point on (X,Y): " + avgLat + " , " + avgLon);
                    }

                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Error " + ex);
        } catch (IOException ex) {
            System.out.println("Error " + ex);
        } finally {
            try {
                fir.close();
            } catch (IOException ex) {
                System.out.println("Error " + ex);
            }
        }

    }

    /**
     * Copied by hand from
     *
     * @see
     * <a href="http://de.wikipedia.org/wiki/Liste_der_Bezirke_und_Statutarst%C3%A4dte_in_%C3%96sterreich">Wikipedia</a>
     */
    static {
        population.put("Wien", 1766746.0);
        population.put("Graz (Stadt)", 269997.0);
        population.put("Linz (Stadt)", 193814.0);
        population.put("Innsbruck-Land", 169680.0);
        population.put("Salzburg (Stadt)", 146631.0);
        population.put("Salzburg-Umgebung", 145275.0);
        population.put("Graz-Umgebung", 144594.0);
        population.put("Linz-Land", 141540.0);
        population.put("Baden", 140078.0);
        population.put("Voecklabruck", 131497.0);
        population.put("Bregenz", 128568.0);
        population.put("Innsbruck-Stadt", 124579.0);
        population.put("Wien-Umgebung", 117343.0);
        population.put("Moedling", 115677.0);
        population.put("Amstetten", 112944.0);
        population.put("Kufstein", 103317.0);
        population.put("Feldkirch", 101497.0);
        population.put("Bruck-Muerzzuschlag", 100855.0);
        population.put("Gmunden", 99540.0);
        population.put("Braunau am Inn", 98842.0);
        population.put("Gaenserndorf", 97460.0);
        population.put("Sankt Poelten (Land)", 97365.0);
        population.put("Gaenserndorf", 97460.0);
        population.put("Klagenfurt Stadt", 96640.0);
        population.put("Hartberg-Fuerstenfeld", 89929.0);
        population.put("Weiz", 88344.0);
        population.put("Suedoststeiermark", 85921.0);
        population.put("Neunkirchen", 85539.0);
        population.put("Zell am See", 84964.0);
        population.put("Dornbirn", 84117.0);
        population.put("Urfahr-Umgebung", 82109.0);
        population.put("Schwaz", 80305.0);
        population.put("Liezen", 79623.0);
        population.put("Sankt Johann im Pongau", 78614.0);
        population.put("Spittal an der Drau", 76971.0);
        population.put("Korneuburg", 76370.0);
        population.put("Melk", 76369.0);
        population.put("Leibnitz", 75775.0);
        population.put("Wiener Neustadt (Land)", 75285.0);
        population.put("Mistelbach", 74150.0);
        population.put("Murtal", 73041.0);
        population.put("Tulln", 72104.0);
        population.put("Wels-Land", 68600.0);
        population.put("Perg", 66269.0);
        population.put("Freistadt", 65208.0);
        population.put("Villach-Land", 64268.0);
        population.put("Grieskirchen", 62938.0);
        population.put("Kitzbuehel", 62318.0);
        population.put("Bludenz", 61100.0);
        population.put("Leoben", 61041.0);
        population.put("Deutschlandsberg", 60466.0);
        population.put("Villach Stadt", 60004.0);
        population.put("Wels (Stadt)", 59339.0);
        population.put("Ried im Innkreis", 58714.0);
        population.put("Steyr-Land", 58618.0);
        population.put("Klagenfurt Land", 58435.0);
        population.put("Hallein", 58336.0);
        population.put("Imst", 57271.0);
        population.put("Neusiedl am See", 56504.0);
        population.put("Rohrbach", 56455.0);
        population.put("Schaerding", 56287.0);
        population.put("Krems an der Donau (Stadt)", 55945.0);
        population.put("Kirchdorf an der Krems", 55571.0);
        population.put("Sankt Veit an der Glan", 55394.0);
        population.put("Oberwart", 53573.0);
        population.put("Wolfsberg", 53472.0);
        population.put("St.Poelten (Stadt)", 52145.0);
        population.put("Wolfsberg", 53472.0);
        population.put("Voitsberg", 51599.0);
        population.put("Hollabrunn", 50065.0);
        population.put("Lienz", 48990.0);
        population.put("Landeck", 43906.0);
        population.put("Bruck an der Leitha", 43615.0);
        population.put("Zwettl", 43102.0);
        population.put("Wiener Neustadt (Stadt)", 42273.0);
        population.put("Voelkermarkt", 42068.0);
        population.put("Eisenstadt-Umgebung", 41474.0);
        population.put("Scheibbs", 41073.0);
        population.put("Mattersburg", 39134.0);
        population.put("Steyr (Stadt)", 38120.0);
        population.put("Oberpullendorf", 37534.0);
        population.put("Gmuend", 37420.0);
        population.put("Eferding", 31961.0);
        population.put("Reutte (Ausserfern)", 31672.0);
        population.put("Horn", 31273.0);
        population.put("Feldkirchen", 30082.0);
        population.put("Murau", 28740.0);
        population.put("Waidhofen an der Thaya", 26424.0);
        population.put("Guessing", 26394.0);
        population.put("Lilienfeld", 26040.0);
        population.put("Krems an der Donau (Stadt)", 24085.0);
        population.put("Tamsweg", 20450.0);
        population.put("Hermagor", 18547.0);
        population.put("Jennersdorf", 17376.0);
        population.put("Eisenstadt (Stadt)", 13485.0);
        population.put("Waidhofen an der Ybbs (Stadt)", 11341.0);
        population.put("Rust (Stadt)", 1942.0);
    }

}
