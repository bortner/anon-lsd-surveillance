/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.datagenerator.analytic;

import at.ac.tuwien.datagenerator.GeoDataCache;
import at.ac.tuwien.datagenerator.util.ValueComparator;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**
 * Visualizes the Shape of the distribution.
 *
 * @author Bernhard
 */
public class HistogramGeoDistribution extends ApplicationFrame {
    
    Map<String, Double> probability = null;
    
    public static void main(String[] args) {
        final HistogramGeoDistribution viz = new HistogramGeoDistribution("GeoData");
        viz.pack();
        RefineryUtilities.centerFrameOnScreen(viz);
        viz.setVisible(true);
    }
    
    public HistogramGeoDistribution(String title) {
        super(title);
        
        final DefaultCategoryDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
    }

    /**
     * Creates the influence distribution shape.
     *
     * @return
     */
    private DefaultCategoryDataset createDataset() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        Map<String, Integer> cnt = new HashMap<>();
        ValueComparator bvc = new ValueComparator(GeoDataCache.PROBABILITY);
        this.probability = new TreeMap<>(bvc);
        probability.putAll(GeoDataCache.PROBABILITY);
        for (int i = 0; i < 1100; i++) {
            choose(cnt);
        }
        for(Map.Entry<String,Integer> entry : cnt.entrySet()){
            Integer val = entry.getValue();
            String key = entry.getKey();
            dataset.setValue(val, "Districts", key);
        }
        
        
        return dataset;
    }

    /**
     * creates the chart itself.
     *
     * @param dataset
     * @return
     */
    private JFreeChart createChart(final DefaultCategoryDataset dataset) {

        // create the chart...
        final JFreeChart chart = ChartFactory.createBarChart(
                "Choosen District", // chart title
                "Name", // x axis label
                "Counted Districts", // y axis label
                dataset, // data
                PlotOrientation.VERTICAL,
                false, // include legend
                true, // tooltips
                false // urls
        );
        
        chart.setBackgroundPaint(Color.white);
        
        final CategoryPlot plot = chart.getCategoryPlot();
        CategoryAxis xAxis =  plot.getDomainAxis();
        xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        final BarRenderer renderer = new BarRenderer();
        plot.setRenderer(renderer);
        
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        return chart;
    }
    
    private void choose(Map<String, Integer> cnt) {
        double rnd = Math.random();
        String selectedShape = null;
        for (Map.Entry<String, Double> entry : probability.entrySet()) {
            String name = entry.getKey();
            Double prob = entry.getValue();
            if (rnd > prob) {
                selectedShape = name;
                break;
            }
        }
        if (cnt.containsKey(selectedShape)) {
            Integer val = cnt.get(selectedShape);
            val++;
            cnt.put(selectedShape, val);
        } else {
            cnt.put(selectedShape, 1);
        }
    }
}
