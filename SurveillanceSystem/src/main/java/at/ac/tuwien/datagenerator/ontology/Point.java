/*
 * generated by http://RDFReactor.semweb4j.org ($Id: CodeGenerator.java 1954 2014-10-29 14:09:16Z roland.stuehmer@fzi.de $) on 12.01.15 21:19
 */
package at.ac.tuwien.datagenerator.ontology;

import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.exception.ModelRuntimeException;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.node.BlankNode;
import org.ontoware.rdf2go.model.node.Node;
import org.ontoware.rdf2go.model.node.Resource;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.ontoware.rdfreactor.runtime.Base;
import org.ontoware.rdfreactor.runtime.ReactorResult;

/**
 * Comment from schema: Uniquely identified by lat/long/alt. i.e.

spaciallyIntersects(P1, P2) :- lat(P1, LAT), long(P1, LONG), alt(P1, ALT),
  lat(P2, LAT), long(P2, LONG), alt(P2, ALT).

sameThing(P1, P2) :- type(P1, Point), type(P2, Point), spaciallyIntersects(P1, P2).
A point, typically described using a coordinate system relative to Earth, such as WGS84.
 *
 * This class manages access to these properties:
 * <ul>
 *   <li>PeopleLongitude</li>
 *   <li>TLatitude</li>
 * </ul>
 *
 * This class was generated by <a href="http://RDFReactor.semweb4j.org">RDFReactor</a> on 12.01.15 21:19
 */
public class Point extends SpatialThing {

    private static final long serialVersionUID = 133022875162247659L;

    /** http://www.w3.org/2003/01/geo/wgs84_pos#Point */
	public static final URI RDFS_CLASS = new URIImpl("http://www.w3.org/2003/01/geo/wgs84_pos#Point", false);

    /** http://isis.tuwien.ac.at/surveillance/People#hasLongitude */
	public static final URI PEOPLELONGITUDE = new URIImpl("http://isis.tuwien.ac.at/surveillance/People#hasLongitude", false);

    /** http://isis.tuwien.ac.at/surveillance/People#hastLatitude */
	public static final URI TLATITUDE = new URIImpl("http://isis.tuwien.ac.at/surveillance/People#hastLatitude", false);

    /**
     * All property-URIs with this class as domain.
     * All properties of all super-classes are also available.
     */
    public static final URI[] MANAGED_URIS = {
      new URIImpl("http://isis.tuwien.ac.at/surveillance/People#hasLongitude", false),
      new URIImpl("http://isis.tuwien.ac.at/surveillance/People#hastLatitude", false)
    };


	// protected constructors needed for inheritance

	/**
	 * Returns a Java wrapper over an RDF object, identified by URI.
	 * Creating two wrappers for the same instanceURI is legal.
	 * @param model RDF2GO Model implementation, see http://rdf2go.semweb4j.org
	 * @param classURI URI of RDFS class
	 * @param instanceIdentifier Resource that identifies this instance
	 * @param write if true, the statement (this, rdf:type, TYPE) is written to the model
	 *
	 * [Generated from RDFReactor template rule #c1]
	 */
	protected Point (Model model, URI classURI, Resource instanceIdentifier, boolean write) {
		super(model, classURI, instanceIdentifier, write);
	}

	// public constructors

	/**
	 * Returns a Java wrapper over an RDF object, identified by URI.
	 * Creating two wrappers for the same instanceURI is legal.
	 * @param model RDF2GO Model implementation, see http://rdf2go.ontoware.org
	 * @param instanceIdentifier an RDF2Go Resource identifying this instance
	 * @param write if true, the statement (this, rdf:type, TYPE) is written to the model
	 *
	 * [Generated from RDFReactor template rule #c2]
	 */
	public Point (Model model, Resource instanceIdentifier, boolean write) {
		super(model, RDFS_CLASS, instanceIdentifier, write);
	}


	/**
	 * Returns a Java wrapper over an RDF object, identified by a URI, given as a String.
	 * Creating two wrappers for the same URI is legal.
	 * @param model RDF2GO Model implementation, see http://rdf2go.ontoware.org
	 * @param uriString a URI given as a String
	 * @param write if true, the statement (this, rdf:type, TYPE) is written to the model
	 * @throws ModelRuntimeException if URI syntax is wrong
	 *
	 * [Generated from RDFReactor template rule #c7]
	 */
	public Point (Model model, String uriString, boolean write) throws ModelRuntimeException {
		super(model, RDFS_CLASS, new URIImpl(uriString,false), write);
	}

	/**
	 * Returns a Java wrapper over an RDF object, identified by a blank node.
	 * Creating two wrappers for the same blank node is legal.
	 * @param model RDF2GO Model implementation, see http://rdf2go.ontoware.org
	 * @param bnode BlankNode of this instance
	 * @param write if true, the statement (this, rdf:type, TYPE) is written to the model
	 *
	 * [Generated from RDFReactor template rule #c8]
	 */
	public Point (Model model, BlankNode bnode, boolean write) {
		super(model, RDFS_CLASS, bnode, write);
	}

	/**
	 * Returns a Java wrapper over an RDF object, identified by
	 * a randomly generated URI.
	 * Creating two wrappers results in different URIs.
	 * @param model RDF2GO Model implementation, see http://rdf2go.ontoware.org
	 * @param write if true, the statement (this, rdf:type, TYPE) is written to the model
	 *
	 * [Generated from RDFReactor template rule #c9]
	 */
	public Point (Model model, boolean write) {
		super(model, RDFS_CLASS, model.newRandomUniqueURI(), write);
	}

    ///////////////////////////////////////////////////////////////////
    // typing

	/**
	 * Return an existing instance of this class in the model. No statements are written.
	 * @param model an RDF2Go model
	 * @param instanceResource an RDF2Go resource
	 * @return an instance of Point or null if none existst
	 *
	 * [Generated from RDFReactor template rule #class0]
	 */
	public static Point getInstance(Model model, Resource instanceResource) {
		return Base.getInstance(model, instanceResource, Point.class);
	}

	/**
	 * Create a new instance of this class in the model.
	 * That is, create the statement (instanceResource, RDF.type, http://www.w3.org/2003/01/geo/wgs84_pos#Point).
	 * @param model an RDF2Go model
	 * @param instanceResource an RDF2Go resource
	 *
	 * [Generated from RDFReactor template rule #class1]
	 */
	public static void createInstance(Model model, Resource instanceResource) {
		Base.createInstance(model, RDFS_CLASS, instanceResource);
	}

	/**
	 * @param model an RDF2Go model
	 * @param instanceResource an RDF2Go resource
	 * @return true if instanceResource is an instance of this class in the model
	 *
	 * [Generated from RDFReactor template rule #class2]
	 */
	public static boolean hasInstance(Model model, Resource instanceResource) {
		return Base.hasInstance(model, RDFS_CLASS, instanceResource);
	}

	/**
	 * @param model an RDF2Go model
	 * @return all instances of this class in Model 'model' as RDF resources
	 *
	 * [Generated from RDFReactor template rule #class3]
	 */
	public static ClosableIterator<Resource> getAllInstances(Model model) {
		return Base.getAllInstances(model, RDFS_CLASS, Resource.class);
	}

	/**
	 * @param model an RDF2Go model
	 * @return all instances of this class in Model 'model' as a ReactorResult,
	 * which can conveniently be converted to iterator, list or array.
	 *
	 * [Generated from RDFReactor template rule #class3-as]
	 */
	public static ReactorResult<? extends Point> getAllInstances_as(Model model) {
		return Base.getAllInstances_as(model, RDFS_CLASS, Point.class );
	}

    /**
	 * Remove triple {@code (this, rdf:type, Point)} from this instance. Other triples are not affected.
	 * To delete more, use deleteAllProperties
	 * @param model an RDF2Go model
	 * @param instanceResource an RDF2Go resource
	 *
	 * [Generated from RDFReactor template rule #class4]
	 */
	public static void deleteInstance(Model model, Resource instanceResource) {
		Base.deleteInstance(model, RDFS_CLASS, instanceResource);
	}

	/**
	 * Delete all triples {@code (this, *, *)}, i.e. including {@code rdf:type}.
	 * @param model an RDF2Go model
	 * @param instanceResource an RDF2Go resource
	 *
	 * [Generated from RDFReactor template rule #class5]
	 */
	public static void deleteAllProperties(Model model,	Resource instanceResource) {
		Base.deleteAllProperties(model, instanceResource);
	}

    ///////////////////////////////////////////////////////////////////
    // property access methods

	/**
	 * @param model an RDF2Go model
	 * @param objectValue
	 * @return all A's as RDF resources, that have a relation 'LocatedIn' to this Point instance
	 *
	 * [Generated from RDFReactor template rule #getallinverse1static]
	 */
	public static ClosableIterator<Resource> getAllPersonLocatedIn_Inverse(Model model, Object objectValue) {
		return Base.getAll_Inverse(model, Thing1.LOCATEDIN, objectValue);
	}

	/**
	 * @return all A's as RDF resources, that have a relation 'LocatedIn' to this Point instance
	 *
	 * [Generated from RDFReactor template rule #getallinverse1dynamic]
	 */
	public ClosableIterator<Resource> getAllPersonLocatedIn_Inverse() {
		return Base.getAll_Inverse(this.model, Thing1.LOCATEDIN, this.getResource() );
	}

	/**
	 * @param model an RDF2Go model
	 * @param objectValue
	 * @return all A's as a ReactorResult, that have a relation 'LocatedIn' to this Point instance
	 *
	 * [Generated from RDFReactor template rule #getallinverse-as1static]
	 */
	public static ReactorResult<Resource> getAllPersonLocatedIn_Inverse_as(Model model, Object objectValue) {
		return Base.getAll_Inverse_as(model, Thing1.LOCATEDIN, objectValue, Resource.class);
	}


	/**
	 * @param model an RDF2Go model
	 * @param objectValue
	 * @return all A's as RDF resources, that have a relation 'Location' to this Point instance
	 *
	 * [Generated from RDFReactor template rule #getallinverse1static]
	 */
	public static ClosableIterator<Resource> getAllPersonLocation_Inverse(Model model, Object objectValue) {
		return Base.getAll_Inverse(model, Person.LOCATION, objectValue);
	}

	/**
	 * @return all A's as RDF resources, that have a relation 'Location' to this Point instance
	 *
	 * [Generated from RDFReactor template rule #getallinverse1dynamic]
	 */
	public ClosableIterator<Resource> getAllPersonLocation_Inverse() {
		return Base.getAll_Inverse(this.model, Person.LOCATION, this.getResource() );
	}

	/**
	 * @param model an RDF2Go model
	 * @param objectValue
	 * @return all A's as a ReactorResult, that have a relation 'Location' to this Point instance
	 *
	 * [Generated from RDFReactor template rule #getallinverse-as1static]
	 */
	public static ReactorResult<Resource> getAllPersonLocation_Inverse_as(Model model, Object objectValue) {
		return Base.getAll_Inverse_as(model, Person.LOCATION, objectValue, Resource.class);
	}



    /**
     * Check if {@code PeopleLongitude} has at least one value set.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return true if this property has at least one value
	 *
	 * [Generated from RDFReactor template rule #get0has-static]
     */
	public static boolean hasPersonPeopleLongitude(Model model, Resource instanceResource) {
		return Base.has(model, instanceResource, PEOPLELONGITUDE);
	}

    /**
     * Check if {@code PeopleLongitude} has at least one value set.
     * @return true if this property has at least one value
	 *
	 * [Generated from RDFReactor template rule #get0has-dynamic]
     */
	public boolean hasPersonPeopleLongitude() {
		return Base.has(this.model, this.getResource(), PEOPLELONGITUDE);
	}

    /**
     * Check if {@code PeopleLongitude} has the given value (maybe among other values).
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be checked
     * @return true if this property contains (maybe among other) the given value
	 *
	 * [Generated from RDFReactor template rule #get0has-value-static]
     */
	public static boolean hasPersonPeopleLongitude(Model model, Resource instanceResource, Node value ) {
		return Base.hasValue(model, instanceResource, PEOPLELONGITUDE, value);
	}

    /**
     * Check if {@code PeopleLongitude} has the given value (maybe among other values).
	 * @param value the value to be checked
     * @return true if this property contains (maybe among other) the given value
	 *
	 * [Generated from RDFReactor template rule #get0has-value-dynamic]
     */
	public boolean hasPersonPeopleLongitude( Node value ) {
		return Base.hasValue(this.model, this.getResource(), PEOPLELONGITUDE, value);
	}

     /**
     * Get all values of property {@code PeopleLongitude} as an Iterator over RDF2Go nodes.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return a ClosableIterator of RDF2Go Nodes
	 *
	 * [Generated from RDFReactor template rule #get7static]
     */
	public static ClosableIterator<Node> getAllPersonPeopleLongitude_asNode(Model model, Resource instanceResource) {
		return Base.getAll_asNode(model, instanceResource, PEOPLELONGITUDE);
	}

    /**
     * Get all values of property {@code PeopleLongitude} as a ReactorResult of RDF2Go nodes.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return a List of RDF2Go Nodes
	 *
	 * [Generated from RDFReactor template rule #get7static-reactor-result]
     */
	public static ReactorResult<Node> getAllPersonPeopleLongitude_asNode_(Model model, Resource instanceResource) {
		return Base.getAll_as(model, instanceResource, PEOPLELONGITUDE, Node.class);
	}

    /**
     * Get all values of property {@code PeopleLongitude} as an Iterator over RDF2Go nodes
     * @return a ClosableIterator of RDF2Go Nodes
	 *
	 * [Generated from RDFReactor template rule #get8dynamic]
     */
	public ClosableIterator<Node> getAllPersonPeopleLongitude_asNode() {
		return Base.getAll_asNode(this.model, this.getResource(), PEOPLELONGITUDE);
	}

    /**
     * Get all values of property {@code PeopleLongitude} as a ReactorResult of RDF2Go nodes.
     * @return a List of RDF2Go Nodes
	 *
	 * [Generated from RDFReactor template rule #get8dynamic-reactor-result]
     */
	public ReactorResult<Node> getAllPersonPeopleLongitude_asNode_() {
		return Base.getAll_as(this.model, this.getResource(), PEOPLELONGITUDE, Node.class);
	}
 
    /**
     * Get all values of property {@code PeopleLongitude}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return a ClosableIterator of $type
	 *
	 * [Generated from RDFReactor template rule #get11static]
     */
	public static ClosableIterator<java.lang.Float> getAllPersonPeopleLongitude(Model model, Resource instanceResource) {
		return Base.getAll(model, instanceResource, PEOPLELONGITUDE, java.lang.Float.class);
	}

    /**
     * Get all values of property {@code PeopleLongitude} as a ReactorResult of {@linkplain java.lang.Float}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return a ReactorResult of $type which can conveniently be converted to iterator, list or array
	 *
	 * [Generated from RDFReactor template rule #get11static-reactorresult]
     */
	public static ReactorResult<java.lang.Float> getAllPersonPeopleLongitude_as(Model model, Resource instanceResource) {
		return Base.getAll_as(model, instanceResource, PEOPLELONGITUDE, java.lang.Float.class);
	}

    /**
     * Get all values of property {@code PeopleLongitude}.
     * @return a ClosableIterator of $type
	 *
	 * [Generated from RDFReactor template rule #get12dynamic]
     */
	public ClosableIterator<java.lang.Float> getAllPersonPeopleLongitude() {
		return Base.getAll(this.model, this.getResource(), PEOPLELONGITUDE, java.lang.Float.class);
	}

    /**
     * Get all values of property {@code PeopleLongitude} as a ReactorResult of {@linkplain java.lang.Float}.
     * @return a ReactorResult of $type which can conveniently be converted to iterator, list or array
	 *
	 * [Generated from RDFReactor template rule #get12dynamic-reactorresult]
     */
	public ReactorResult<java.lang.Float> getAllPersonPeopleLongitude_as() {
		return Base.getAll_as(this.model, this.getResource(), PEOPLELONGITUDE, java.lang.Float.class);
	}

 
    /**
     * Adds a value to property {@code PeopleLongitude} as an RDF2Go {@linkplain Node}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #add1static]
     */
	public static void addPersonPeopleLongitude(Model model, Resource instanceResource, Node value) {
		Base.add(model, instanceResource, PEOPLELONGITUDE, value);
	}

    /**
     * Adds a value to property {@code PeopleLongitude} as an RDF2Go {@linkplain Node}.
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #add1dynamic]
     */
	public void addPersonPeopleLongitude(Node value) {
		Base.add(this.model, this.getResource(), PEOPLELONGITUDE, value);
	}
    /**
     * Adds a value to property {@code PeopleLongitude} from an instance of {@linkplain java.lang.Float}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @param value
	 *
	 * [Generated from RDFReactor template rule #add3static]
     */
	public static void addPersonPeopleLongitude(Model model, Resource instanceResource, java.lang.Float value) {
		Base.add(model, instanceResource, PEOPLELONGITUDE, value);
	}

    /**
     * Adds a value to property {@code PeopleLongitude} from an instance of {@linkplain java.lang.Float}.
	 *
	 * [Generated from RDFReactor template rule #add4dynamic]
     */
	public void addPersonPeopleLongitude(java.lang.Float value) {
		Base.add(this.model, this.getResource(), PEOPLELONGITUDE, value);
	}
  

    /**
     * Sets a value of property {@code PeopleLongitude} from an RDF2Go {@linkplain Node}.
     * First, all existing values are removed, then this value is added.
     * Cardinality constraints are not checked, but this method exists only for properties with
     * no {@code minCardinality} or {@code minCardinality == 1}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be set
	 *
	 * [Generated from RDFReactor template rule #set1static]
     */
	public static void setPersonPeopleLongitude(Model model, Resource instanceResource, Node value) {
		Base.set(model, instanceResource, PEOPLELONGITUDE, value);
	}

    /**
     * Sets a value of property {@code PeopleLongitude} from an RDF2Go {@linkplain Node}.
     * First, all existing values are removed, then this value is added.
     * Cardinality constraints are not checked, but this method exists only for properties with
     * no {@code minCardinality} or {@code minCardinality == 1}.
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #set1dynamic]
     */
	public void setPersonPeopleLongitude(Node value) {
		Base.set(this.model, this.getResource(), PEOPLELONGITUDE, value);
	}
    /**
     * Sets a value of property {@code PeopleLongitude} from an instance of {@linkplain java.lang.Float}.
     * First, all existing values are removed, then this value is added.
     * Cardinality constraints are not checked, but this method exists only for properties with
     * no {@code minCardinality} or {@code minCardinality == 1}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #set3static]
     */
	public static void setPersonPeopleLongitude(Model model, Resource instanceResource, java.lang.Float value) {
		Base.set(model, instanceResource, PEOPLELONGITUDE, value);
	}

    /**
     * Sets a value of property {@code PeopleLongitude} from an instance of {@linkplain java.lang.Float}.
     * First, all existing values are removed, then this value is added.
     * Cardinality constraints are not checked, but this method exists only for properties with
     * no {@code minCardinality} or {@code minCardinality == 1}.
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #set4dynamic]
     */
	public void setPersonPeopleLongitude(java.lang.Float value) {
		Base.set(this.model, this.getResource(), PEOPLELONGITUDE, value);
	}
  


    /**
     * Removes a value of property {@code PeopleLongitude} as an RDF2Go {@linkplain Node}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be removed
	 *
	 * [Generated from RDFReactor template rule #remove1static]
     */
	public static void removePersonPeopleLongitude(Model model, Resource instanceResource, Node value) {
		Base.remove(model, instanceResource, PEOPLELONGITUDE, value);
	}

    /**
     * Removes a value of property {@code PeopleLongitude} as an RDF2Go {@linkplain Node}.
	 * @param value the value to be removed
	 *
	 * [Generated from RDFReactor template rule #remove1dynamic]
     */
	public void removePersonPeopleLongitude(Node value) {
		Base.remove(this.model, this.getResource(), PEOPLELONGITUDE, value);
	}
    /**
     * Removes a value of property {@code PeopleLongitude} given as an instance of {@linkplain java.lang.Float}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be removed
	 *
	 * [Generated from RDFReactor template rule #remove3static]
     */
	public static void removePersonPeopleLongitude(Model model, Resource instanceResource, java.lang.Float value) {
		Base.remove(model, instanceResource, PEOPLELONGITUDE, value);
	}

    /**
     * Removes a value of property {@code PeopleLongitude} given as an instance of {@linkplain java.lang.Float}.
	 * @param value the value to be removed
	 *
	 * [Generated from RDFReactor template rule #remove4dynamic]
     */
	public void removePersonPeopleLongitude(java.lang.Float value) {
		Base.remove(this.model, this.getResource(), PEOPLELONGITUDE, value);
	}
  
    /**
     * Removes all values of property {@code PeopleLongitude}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 *
	 * [Generated from RDFReactor template rule #removeall1static]
     */
	public static void removeAllPersonPeopleLongitude(Model model, Resource instanceResource) {
		Base.removeAll(model, instanceResource, PEOPLELONGITUDE);
	}

    /**
     * Removes all values of property {@code PeopleLongitude}.
	 *
	 * [Generated from RDFReactor template rule #removeall1dynamic]
     */
	public void removeAllPersonPeopleLongitude() {
		Base.removeAll(this.model, this.getResource(), PEOPLELONGITUDE);
	}
     /**
     * Check if {@code TLatitude} has at least one value set.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return true if this property has at least one value
	 *
	 * [Generated from RDFReactor template rule #get0has-static]
     */
	public static boolean hasPersonTLatitude(Model model, Resource instanceResource) {
		return Base.has(model, instanceResource, TLATITUDE);
	}

    /**
     * Check if {@code TLatitude} has at least one value set.
     * @return true if this property has at least one value
	 *
	 * [Generated from RDFReactor template rule #get0has-dynamic]
     */
	public boolean hasPersonTLatitude() {
		return Base.has(this.model, this.getResource(), TLATITUDE);
	}

    /**
     * Check if {@code TLatitude} has the given value (maybe among other values).
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be checked
     * @return true if this property contains (maybe among other) the given value
	 *
	 * [Generated from RDFReactor template rule #get0has-value-static]
     */
	public static boolean hasPersonTLatitude(Model model, Resource instanceResource, Node value ) {
		return Base.hasValue(model, instanceResource, TLATITUDE, value);
	}

    /**
     * Check if {@code TLatitude} has the given value (maybe among other values).
	 * @param value the value to be checked
     * @return true if this property contains (maybe among other) the given value
	 *
	 * [Generated from RDFReactor template rule #get0has-value-dynamic]
     */
	public boolean hasPersonTLatitude( Node value ) {
		return Base.hasValue(this.model, this.getResource(), TLATITUDE, value);
	}

     /**
     * Get all values of property {@code TLatitude} as an Iterator over RDF2Go nodes.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return a ClosableIterator of RDF2Go Nodes
	 *
	 * [Generated from RDFReactor template rule #get7static]
     */
	public static ClosableIterator<Node> getAllPersonTLatitude_asNode(Model model, Resource instanceResource) {
		return Base.getAll_asNode(model, instanceResource, TLATITUDE);
	}

    /**
     * Get all values of property {@code TLatitude} as a ReactorResult of RDF2Go nodes.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return a List of RDF2Go Nodes
	 *
	 * [Generated from RDFReactor template rule #get7static-reactor-result]
     */
	public static ReactorResult<Node> getAllPersonTLatitude_asNode_(Model model, Resource instanceResource) {
		return Base.getAll_as(model, instanceResource, TLATITUDE, Node.class);
	}

    /**
     * Get all values of property {@code TLatitude} as an Iterator over RDF2Go nodes
     * @return a ClosableIterator of RDF2Go Nodes
	 *
	 * [Generated from RDFReactor template rule #get8dynamic]
     */
	public ClosableIterator<Node> getAllPersonTLatitude_asNode() {
		return Base.getAll_asNode(this.model, this.getResource(), TLATITUDE);
	}

    /**
     * Get all values of property {@code TLatitude} as a ReactorResult of RDF2Go nodes.
     * @return a List of RDF2Go Nodes
	 *
	 * [Generated from RDFReactor template rule #get8dynamic-reactor-result]
     */
	public ReactorResult<Node> getAllPersonTLatitude_asNode_() {
		return Base.getAll_as(this.model, this.getResource(), TLATITUDE, Node.class);
	}
 
    /**
     * Get all values of property {@code TLatitude}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return a ClosableIterator of $type
	 *
	 * [Generated from RDFReactor template rule #get11static]
     */
	public static ClosableIterator<java.lang.Float> getAllPersonTLatitude(Model model, Resource instanceResource) {
		return Base.getAll(model, instanceResource, TLATITUDE, java.lang.Float.class);
	}

    /**
     * Get all values of property {@code TLatitude} as a ReactorResult of {@linkplain java.lang.Float}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @return a ReactorResult of $type which can conveniently be converted to iterator, list or array
	 *
	 * [Generated from RDFReactor template rule #get11static-reactorresult]
     */
	public static ReactorResult<java.lang.Float> getAllPersonTLatitude_as(Model model, Resource instanceResource) {
		return Base.getAll_as(model, instanceResource, TLATITUDE, java.lang.Float.class);
	}

    /**
     * Get all values of property {@code TLatitude}.
     * @return a ClosableIterator of $type
	 *
	 * [Generated from RDFReactor template rule #get12dynamic]
     */
	public ClosableIterator<java.lang.Float> getAllPersonTLatitude() {
		return Base.getAll(this.model, this.getResource(), TLATITUDE, java.lang.Float.class);
	}

    /**
     * Get all values of property {@code TLatitude} as a ReactorResult of {@linkplain java.lang.Float}.
     * @return a ReactorResult of $type which can conveniently be converted to iterator, list or array
	 *
	 * [Generated from RDFReactor template rule #get12dynamic-reactorresult]
     */
	public ReactorResult<java.lang.Float> getAllPersonTLatitude_as() {
		return Base.getAll_as(this.model, this.getResource(), TLATITUDE, java.lang.Float.class);
	}

 
    /**
     * Adds a value to property {@code TLatitude} as an RDF2Go {@linkplain Node}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #add1static]
     */
	public static void addPersonTLatitude(Model model, Resource instanceResource, Node value) {
		Base.add(model, instanceResource, TLATITUDE, value);
	}

    /**
     * Adds a value to property {@code TLatitude} as an RDF2Go {@linkplain Node}.
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #add1dynamic]
     */
	public void addPersonTLatitude(Node value) {
		Base.add(this.model, this.getResource(), TLATITUDE, value);
	}
    /**
     * Adds a value to property {@code TLatitude} from an instance of {@linkplain java.lang.Float}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
     * @param value
	 *
	 * [Generated from RDFReactor template rule #add3static]
     */
	public static void addPersonTLatitude(Model model, Resource instanceResource, java.lang.Float value) {
		Base.add(model, instanceResource, TLATITUDE, value);
	}

    /**
     * Adds a value to property {@code TLatitude} from an instance of {@linkplain java.lang.Float}.
	 *
	 * [Generated from RDFReactor template rule #add4dynamic]
     */
	public void addPersonTLatitude(java.lang.Float value) {
		Base.add(this.model, this.getResource(), TLATITUDE, value);
	}
  

    /**
     * Sets a value of property {@code TLatitude} from an RDF2Go {@linkplain Node}.
     * First, all existing values are removed, then this value is added.
     * Cardinality constraints are not checked, but this method exists only for properties with
     * no {@code minCardinality} or {@code minCardinality == 1}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be set
	 *
	 * [Generated from RDFReactor template rule #set1static]
     */
	public static void setPersonTLatitude(Model model, Resource instanceResource, Node value) {
		Base.set(model, instanceResource, TLATITUDE, value);
	}

    /**
     * Sets a value of property {@code TLatitude} from an RDF2Go {@linkplain Node}.
     * First, all existing values are removed, then this value is added.
     * Cardinality constraints are not checked, but this method exists only for properties with
     * no {@code minCardinality} or {@code minCardinality == 1}.
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #set1dynamic]
     */
	public void setPersonTLatitude(Node value) {
		Base.set(this.model, this.getResource(), TLATITUDE, value);
	}
    /**
     * Sets a value of property {@code TLatitude} from an instance of {@linkplain java.lang.Float}.
     * First, all existing values are removed, then this value is added.
     * Cardinality constraints are not checked, but this method exists only for properties with
     * no {@code minCardinality} or {@code minCardinality == 1}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #set3static]
     */
	public static void setPersonTLatitude(Model model, Resource instanceResource, java.lang.Float value) {
		Base.set(model, instanceResource, TLATITUDE, value);
	}

    /**
     * Sets a value of property {@code TLatitude} from an instance of {@linkplain java.lang.Float}.
     * First, all existing values are removed, then this value is added.
     * Cardinality constraints are not checked, but this method exists only for properties with
     * no {@code minCardinality} or {@code minCardinality == 1}.
	 * @param value the value to be added
	 *
	 * [Generated from RDFReactor template rule #set4dynamic]
     */
	public void setPersonTLatitude(java.lang.Float value) {
		Base.set(this.model, this.getResource(), TLATITUDE, value);
	}
  


    /**
     * Removes a value of property {@code TLatitude} as an RDF2Go {@linkplain Node}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be removed
	 *
	 * [Generated from RDFReactor template rule #remove1static]
     */
	public static void removePersonTLatitude(Model model, Resource instanceResource, Node value) {
		Base.remove(model, instanceResource, TLATITUDE, value);
	}

    /**
     * Removes a value of property {@code TLatitude} as an RDF2Go {@linkplain Node}.
	 * @param value the value to be removed
	 *
	 * [Generated from RDFReactor template rule #remove1dynamic]
     */
	public void removePersonTLatitude(Node value) {
		Base.remove(this.model, this.getResource(), TLATITUDE, value);
	}
    /**
     * Removes a value of property {@code TLatitude} given as an instance of {@linkplain java.lang.Float}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 * @param value the value to be removed
	 *
	 * [Generated from RDFReactor template rule #remove3static]
     */
	public static void removePersonTLatitude(Model model, Resource instanceResource, java.lang.Float value) {
		Base.remove(model, instanceResource, TLATITUDE, value);
	}

    /**
     * Removes a value of property {@code TLatitude} given as an instance of {@linkplain java.lang.Float}.
	 * @param value the value to be removed
	 *
	 * [Generated from RDFReactor template rule #remove4dynamic]
     */
	public void removePersonTLatitude(java.lang.Float value) {
		Base.remove(this.model, this.getResource(), TLATITUDE, value);
	}
  
    /**
     * Removes all values of property {@code TLatitude}.
     * @param model an RDF2Go model
     * @param instanceResource an RDF2Go resource
	 *
	 * [Generated from RDFReactor template rule #removeall1static]
     */
	public static void removeAllPersonTLatitude(Model model, Resource instanceResource) {
		Base.removeAll(model, instanceResource, TLATITUDE);
	}

    /**
     * Removes all values of property {@code TLatitude}.
	 *
	 * [Generated from RDFReactor template rule #removeall1dynamic]
     */
	public void removeAllPersonTLatitude() {
		Base.removeAll(this.model, this.getResource(), TLATITUDE);
	}
 }