/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.lod;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Bernhard
 */
public abstract class SparqlEndpoint {

    private final Logger logger = LoggerFactory.getLogger(SparqlEndpoint.class);
    /**
     * set by the instance implementation
     */
    String url = "";

    /**
     * Executes a given Sparql Query against a given endpoint. Returns a Map of
     * {variable -> Objects} of the results. Objects are either a List of single
     * Values or Single values itself.
     *
     * @param query
     * @return parsed Resultset
     */
    public Map<String, ?> queryEndpoint(String query) {
        //in my case its always a String -> String Mapping
        Map<String, Object> results = new HashMap<>();
        logger.trace("Query: \n" + query);
        QueryExecution qe = QueryExecutionFactory.sparqlService(url, query);
        ResultSet rs = qe.execSelect();
        while (rs.hasNext()) {
            QuerySolution sol = rs.next();
            Iterator<String> varNames = sol.varNames();
            while (varNames.hasNext()) {
                String var = varNames.next();
                RDFNode curNode = sol.get(var);
                if (curNode.isLiteral()) {
                    results.put(var, curNode.asNode().getLiteral().getValue());
                } else {
                    results.put(var, curNode.asResource().getLocalName());
                }
            }
        }
        return results;
    }

    /**
     * Return the implicitly ordered Attributes, i.e. 0 is the lowest
     * granularity and n the highest. For example: <br>
     * 0.. Street with house-numbers <br>
     * 1.. Street<br>
     * .
     * .<br>
     * n.. country<br>
     *
     * @return a List of hierarchical ordered attributes (Default: empty List).
     */
    public List<String> buildHierarchy() {
        return Collections.emptyList();
    }

    /**
     * Tests if a certain endpoint is up or down.
     *
     * @return true if an endpoint is up, false otherwise.
     */
    public boolean testEndpoint() {
        boolean up = false;
        String query = "ASK { }";
        QueryExecution qe = QueryExecutionFactory.sparqlService(url, query);
        try {
            if (qe.execAsk()) {
                up = true;
            } // end if
        } catch (QueryExceptionHTTP e) {
            up = false;
        } finally {
            qe.close();
        } // end try/catch/finally
        return up;
    }
}
