/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine;

import at.ac.tuwien.engine.lod.BioPortal;
import at.ac.tuwien.engine.lod.GeoNames;
import at.ac.tuwien.engine.util.events.AnonEvent;
import at.ac.tuwien.engine.util.events.ILEvent;
import at.ac.tuwien.engine.util.EventBus;
import at.ac.tuwien.engine.util.Pair;
import at.ac.tuwien.engine.util.SetPartition;
import at.ac.tuwien.engine.util.SubsetEnumerator;
import at.ac.tuwien.engine.util.events.CCEvent;
import at.ac.tuwien.sursystem.model.Address;
import at.ac.tuwien.sursystem.model.ClinicalCondition;
import at.ac.tuwien.sursystem.model.Cluster;
import at.ac.tuwien.sursystem.model.Datapoint;
import at.ac.tuwien.sursystem.model.Person;
import at.ac.tuwien.sursystem.model.SubCluster;
import com.pusher.rest.Pusher;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import org.apache.commons.lang3.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

/**
 * This service clusters all incoming data.
 *
 * @author Bernhard
 */
public class ClusterService {

    /**
     * k-Anonymity. The k implies that all values below a certain k are skipped.
     * As a result they try to form a subcluster, by using appropriate
     * pre-generalization steps. (generalizing different streets to districts,
     * countries,...)
     */
    private static int k;
    /**
     * l-diversity. Requires k-Anonymity. A cluster is said to be l-diverse iff
     * l-distinct values of the sensitive attribute.
     */
    private static int l;
    /**
     * The QID Test mechanism.
     */
    private final static SetPartition setPartition = new SetPartition();

    /**
     * Builds subsets of a certain cluster.
     */
    private final static SubsetEnumerator enumerator = new SubsetEnumerator();

    // <editor-fold defaultstate="collapsed" desc="Hierarchy Initializing">
    /**
     * there What hierarchy obtained from bioportal.
     */
    private static List<String> whatHierarchy;
    /**
     * the Where hierarchy obtained from geoNames.
     */
    private static List<String> whereHierarchy;

    /**
     * The GeoNames Service for retrieving all Geo-Data. Necessary because I
     * just generate <Lat,Lng> Pairs.
     */
    private static final GeoNames names = new GeoNames();

    /**
     * Initalize the hierarchies.
     */
    static {
        BioPortal portal = new BioPortal();
        whatHierarchy = portal.buildHierarchy();
        whereHierarchy = names.buildHierarchy();
    }

    // </editor-fold>
    private final static Logger logger = LoggerFactory.getLogger(ClusterService.class);

    // <editor-fold defaultstate="collapsed" desc="Caches">
    /**
     * The local cache for minimizing the amount of queries to GeoNames.
     */
    private static Map<Pair, Address> adrCache = new HashMap<>();

    private static Map<String, Map<String, Float>> strCache = new HashMap<>();
    // </editor-fold>
    /**
     * Cluster for the where dimension. Requires the
     * {@link ClusterService#whereHierarchy}.
     */
    private Cluster whereCluster = null;
    /**
     * Cluster for the what dimension. Requires the
     * {@link ClusterService#whatHierarchy}.
     */
    private Cluster whatCluster = null;

    private Cluster whoCluster = null;

    private Cluster whenCluster = null;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private Map<String, Object> clusters2Entity = new HashMap<>();
    /**
     * The delta constraint, when a cluster is expired. When a cluster is
     * expired, it is released immeditaley. Setting the delta to 0 means that
     * the cluster is released in real time (by default it is 0).
     */
    private long delta = 0L;

    private static transient String DATE_PATTERN = "^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$";

    public ClusterService() {
    }

    /**
     * Desired Constructor. Applies local Anonymization. The senstive attribute
     * is defined as a dimension.
     *
     * @param k for k Anonymity
     * @param l for l diversity
     * @param delta in Miliseconds, when a cluster expires
     */
    public ClusterService(int k, int l, long delta) {
        this.k = k;
        this.l = l;
        this.delta = delta;
        whereCluster = new Cluster(whereHierarchy, k, l);
        whatCluster = new Cluster(whatHierarchy, k, l);
        whoCluster = new Cluster(new ArrayList<String>(), k, l);
        whenCluster = new Cluster(new ArrayList<String>(), k, l);
        //the hard cluster constraint...
        //it should write to the Guava Event Bus.
        Timer timer = new Timer("deltaConstraints", true);
        timer.scheduleAtFixedRate(new TimerTask() {

            /**
             * Releases anonymized Data on {@link Pusher}. After releasing it
             * Dimensional Data will be deleted.
             */
            @Override
            public void run() {
                logger.info("delta constraint is expired; Releasing clusters");
                Map where = whereCluster.getIndexedProperties();
                Map what = whatCluster.getIndexedProperties();
                Map who = whoCluster.getIndexedProperties();
                Map when = whenCluster.getIndexedProperties();

                for (int i = 0; i < where.size(); i++) {
                    String geoStuff = where.get(i).toString();
                    Address adr = null;
                    if (!geoStuff.contains("(")) {
                        Float lng = null;
                        Float lat = null;
                        if (!strCache.containsKey(geoStuff)) {
                            StrBuilder qry = new StrBuilder();
                            qry.append(names.resolveProperty);
                            qry.replaceAll("$search", geoStuff);
                            //add caching here !
                            Map<String, ?> result = names.queryEndpoint(qry.toString());
                            if (result.get("lng") == null) {
                                continue;
                            }
                            lng = new Float(result.get("lng").toString());
                            lat = new Float(result.get("lat").toString());
                            Map<String, Float> location = new HashMap<>();
                            location.put("lng", lng);
                            location.put("lat", lat);
                            strCache.put(geoStuff, location);
                            logger.trace(geoStuff + " was resolved by " + lng + " and " + lat);
                        } else {
                            Map<String, Float> p = strCache.get(geoStuff);
                            lat = p.get("lat");
                            lng = p.get("lng");
                        }
                        adr = new Address(lat, lng);
                    } else {
                        adr = (Address) where.get(i);
                    }
                    ClinicalCondition cnd = new ClinicalCondition();
                    if (!what.isEmpty() && what.get(i) != null) {
                        Object ili = what.get(i);
                        if (ili instanceof String) {
                            cnd.setSickness(what.get(i));
                        } else {
                            cnd = (ClinicalCondition) ili;
                        }
                    }
                    Object o = who.get(i);
                    Person psn = new Person();
                    if (o != null) {
                        psn = (Person) o;
                    }
                    Date time = new Date();
                    if (!when.isEmpty() && when.get(i) != null) {
                        try {
                            time = sdf.parse(when.get(i).toString());
                        } catch (Exception ex) {
                        }
                    }
                    Datapoint dp = new Datapoint();
                    dp.setWhere(adr);
                    dp.setWhat(cnd);
                    dp.setWho(psn);
                    dp.setWhen(time);
                    logger.info("{}", dp);
                    EventBus.getInstance().post(new AnonEvent(dp));
                }

                //collect InformationLoss
                Float whoIL = whoCluster.getInformationLoss();
                Float whatIL = whatCluster.getInformationLoss();
                Float whereIL = whereCluster.getInformationLoss();
                Float informationLoss = whoIL + whatIL + whereIL;
                EventBus.getInstance().post(new ILEvent(informationLoss));

                Integer removedWhos = whoCluster.releaseCluster();
                Integer removedWhats = whatCluster.releaseCluster();
                Integer removedWheres = whereCluster.releaseCluster();
                EventBus.getInstance().post(new CCEvent((removedWhats + removedWheres + removedWhos)));
            }
        }, delta, delta);

    }

    /**
     * Classifies a given datapoint within the available list of clusters.
     * Workflow: 1) add a datapoint to the corresponding cluster 2) detect all
     * QIDs 3) generalize it (or not)
     *
     * @param dp
     */
    public synchronized void classify(Datapoint dp) {
        resolveWhere(dp.getWhere());
        //pre Step resolve the address via BioPortal
        Address whereResolved = resolveWhere(dp.getWhere());
        logger.trace(whereResolved.getCity());
        String whereKey = whereCluster.addDataVector(whereResolved);
        if (!clusters2Entity.containsKey(whereKey)) {
            clusters2Entity.put(whereKey, whereResolved);
        }

        ClinicalCondition whatResolved = dp.getWhat();
        String whatKey = whatCluster.addDataVector(whatResolved);
        if (!clusters2Entity.containsKey(whatKey)) {
            clusters2Entity.put(whatKey, whatResolved);
        }
        Person whoResolved = dp.getWho();
        String whoKey = whoCluster.addDataVector(whoResolved);
        if (!clusters2Entity.containsKey(whoKey)) {
            clusters2Entity.put(whoKey, whoResolved);
        }
        //anonymize the clusters
        String whenKey = whenCluster.addDataVector(dp.getWhen());

        whoCluster.generalizeSubCluster(whoKey);
        whereCluster.generalizeSubCluster(whereKey);
        whatCluster.generalizeSubCluster(whatKey);
        whenCluster.generalizeSubCluster(whenKey);

        //workflow would be like this.
        //baseset for all Combinations (Universe/Domain)     
        Set<Pair> baseset = new HashSet<>();
        //Combinations of the whereDimenion, test all Combinations of Where
        Set<Pair> subsetWhere = enumerator.generateSubset(whereCluster.getIndexedProperties());
        //Combinations of the what Dimension, test all Combinations of What
        Set<Pair> subsetWhat = enumerator.generateSubset(whatCluster.getIndexedProperties());
        Set<Pair> subsetWho = enumerator.generateSubset(whoCluster.getIndexedProperties());
        List<Set<Pair>> subsets = new ArrayList<>();
        subsets.add(subsetWhere);
        subsets.add(subsetWhat);
        subsets.add(subsetWho);
        baseset.addAll(subsetWhere);
        //Set Partition Problem for finding all QIDs
        Set<Set<Set<Pair>>> partitions = setPartition.findCombination(baseset, subsets);
        //apply anonymization algorithms
        for (Set<Set<Pair>> partition : partitions) {
            for (Set<Pair> par : partition) {
                if (!par.isEmpty()) {
                    //if the set equals the where-set    
                    if (par.equals(subsetWhere)) {
                        logger.debug("Anonymizing Where-Dimension");
                        //merge those cluster which do not fulfill k-Anonymity
                        List<SubCluster> mergeCandidates = whereCluster.getMergeCandidates();
                        SubCluster cl = new SubCluster();
                        for (SubCluster c : mergeCandidates) {
                            List rawData = c.getRawData();
                            for (Object o : rawData) {
                                cl.addRawData(o);
                            }
                        }
                        cl.setAnonLevel(SubCluster.GLOBAL_ANONYMIZATION, whereHierarchy.size());
                        whereCluster.addSubCluster("Merged", cl);
                        whereCluster.generalizeSubCluster("Merged");

                        //generalize one Step - think about deanonymization
                        whereCluster.generalizeSubCluster(whereResolved.toString());

                        if (logger.isDebugEnabled()) {
                            Map clusters = whereCluster.getIndexedProperties();
                            logger.debug("Analytics\n " + clusters);
                        }
                    }
                    if (par.equals(subsetWhat)) {
                        logger.debug("Anonymizing What-Dimension");
                        List<SubCluster> mergeCandidates = whatCluster.getMergeCandidates();
                        SubCluster cl = new SubCluster();
                        for (SubCluster c : mergeCandidates) {
                            List rawData = c.getRawData();
                            for (Object o : rawData) {
                                cl.addRawData(o);
                            }
                        }
                        whatCluster.addSubCluster("Merged3", cl);
                        whatCluster.generalizeSubCluster("Merged3");
                        whatCluster.generalizeSubCluster(whatResolved.toString());

                        if (logger.isDebugEnabled()) {
                            Map clusters = whatCluster.getIndexedProperties();
                            logger.debug("Analytics\n " + clusters);
                        }
                    }
                    if (par.equals(subsetWho)) {
                        logger.debug("Anonymizing Who-Dimension");
                        List<SubCluster> mergeCandidates = whoCluster.getMergeCandidates();
                        SubCluster cl = new SubCluster();
                        for (SubCluster c : mergeCandidates) {
                            List rawData = c.getRawData();
                            for (Object o : rawData) {
                                cl.addRawData(o);
                            }
                        }
                        whoCluster.addSubCluster("Merged2", cl);
                        whoCluster.generalizeSubCluster("Merged2");
                        whoCluster.generalizeSubCluster(whoResolved.toString());

                        if (logger.isDebugEnabled()) {
                            Map clusters = whoCluster.getIndexedProperties();
                            logger.debug("Analytics\n " + clusters);
                        }
                    }
                }
            }
        }
    }

    /**
     * get a unique Identifier by Reflection. ##TODO
     *
     * @param dp
     * @param field
     * @return
     */
    protected String getKey(Datapoint dp, String field) {
        String key = "";
        try {
            Class<?> clazz = dp.getClass();
            Field findField = ReflectionUtils.findField(clazz, field);
            Object value = findField.get(dp);
            key = value.toString();
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            logger.error(ex.getLocalizedMessage(), ex);
        }
        return key;
    }

    /**
     * Detect all Missing Values from the LOD Cloud. Convenience Method for the
     * data generator.
     *
     * @param address
     * @return
     */
    protected Address resolveWhere(Address address) {
        Pair p = new Pair(address.getLat(), address.getLng());
        if (adrCache.containsKey(p)) {
            return adrCache.get(p);
        }
        String query = names.jenaQuery.replace("$lat", address.getLat().toString());
        query = query.replace("$lng", address.getLng().toString());
        Map<String, ?> geoRes = names.queryEndpoint(query);
        if (geoRes.isEmpty()) {
            System.out.println("Lat: " + address.getLat() + " , Lng: " + address.getLng());
            logger.debug(query);
        }
        address.setCountry(geoRes.get("country").toString());
        address.setStreet(geoRes.get("street").toString());
        address.setLat(Float.valueOf(geoRes.get("lat").toString()));
        address.setLng(Float.valueOf(geoRes.get("lng").toString()));
        address.setDistrict(geoRes.get("district").toString());
        address.setCity(geoRes.get("city").toString());
        adrCache.put(p, address);
        return address;
    }

    public static boolean testDate(String date) {
        return date.matches(DATE_PATTERN);
    }

}
