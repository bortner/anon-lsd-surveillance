/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * This is the "Set Partition Problem" that identifies all QIDs. <br>
 * This problem is one of Karp's 21 Problems, i.e. it is NP hard.
 * <p>
 * It is based on the "min set cover problem" but altered to fit the SPP. The 
 * basis implementation for Set-Cover-Problem was found on stackoverflow.
 *
 * @see
 * <a href="http://stackoverflow.com/questions/4287302/find-combinations-of-all-sets-set-cover">Base
 * Set Cover</a>
 * @author Bernhard
 */
public class SetPartition<T> {

    interface Filter<T> {

        boolean matches(T t);
    }

    /**
     * Calculates the Set-Cover for given Sets.
     *
     * @param base
     * @param subsets
     * @return
     */
    public Set<?> findCombination(final Set<T> base, List<Set<T>> subsets) {

        Filter<Set<Set<T>>> filter = new Filter<Set<Set<T>>>() {
            @Override
            public boolean matches(Set<Set<T>> integers) {
                //merges all given sets and tests if they match the base set.
                Set<T> union = new LinkedHashSet<>();
                for (Set<T> ints : integers) {
                    union.addAll(ints);
                }
                return union.equals(base);
            }
        };

        Set<?> firstSolution = shortestCombination(filter, subsets);
        return firstSolution;
    }

    /**
     * Detect the shortest combination of all input sets.
     *
     * @param <T>
     * @param filter
     * @param listOfSets
     * @return
     */
    private <T> Set<?> shortestCombination(Filter<Set<T>> filter, List<T> listOfSets) {
        Set<Set<T>> result = new LinkedHashSet<>();
        final int size = listOfSets.size();
        if (size > 20) {
            throw new IllegalArgumentException("Too many combinations");
        }
        //Kombinationen ohne Wiederholung: 6 
        //shift 1, 3 bits left, because there are (n k) combinations (i.e. 8)
        int combinations = 1 << size;
        List<Set<T>> possibleSolutions = new ArrayList<>();
        for (int l = 0; l < combinations; l++) {
            //generates all  combinations 
            Set<T> combination = new LinkedHashSet<>();
            //check if the generated solution is empty or the base set.
            for (int j = 0; j < size; j++) {
                //bitwise right operation j times and delete the lsb
                //inverse function to above (move 3 bits right and remove 1)
                int curElement = ((l >> j) & 1);
                if (curElement != 0) {
                    combination.add(listOfSets.get(j));
                }
            }
            if ((0 < combination.size()) && (combination.size() < size)) {
                possibleSolutions.add(combination);
        }
        }
        // the possible solutions in order of size.
        Collections.sort(possibleSolutions, new Comparator<Set<T>>() {
            @Override
            public int compare(Set<T> o1, Set<T> o2) {
                return o1.size() - o2.size();
            }
        });
        for (Set<T> possibleSolution : possibleSolutions) {
            boolean valid = filter.matches(possibleSolution);
            if (valid) {
                //todo Shrink the solution, i.e. remove all QIDs Solution
                result.add(possibleSolution);
            }
        }
        return result;
    }
}
