/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Based on "Anonymization of Electronic Medical Records to Support Clinical Anaylsis.
 * 
 * https://books.google.at/books?id=NsETVho25A0C&pg=PA49&lpg=PA49&dq=information+loss+metric&source=bl&ots=RDOHcZTLZ_&sig=iOmogsc-EGZKcNJ3clV5XOKoZP8&hl=de&sa=X&ei=1BLqVOmMOsT0UKzCgYgN&ved=0CDcQ6AEwBDgK#v=onepage&q=information%20loss%20metric&f=false
 * @author Bernhard
 */
public class InformationLossMetric {

    /**
     * The penalty terms for suppressed values.
     */
    private static float weight = 0F; 
    
    /**
     * The matrix that contains the <Timestamp, Amount of IL> measurements.
     */
    private Map<Long, Float> ilMatrice = new HashMap<>();
    
    /**
     * Constructor. Adds a weight for suppressed or missing terms.
     * @param weight
     */
    public InformationLossMetric(float weight) {
        this.weight = weight;
    }
    
    
    
    
    
}
