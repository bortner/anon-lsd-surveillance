/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.util;

import at.ac.tuwien.engine.util.events.AnonEvent;
import at.ac.tuwien.engine.util.events.CCEvent;
import at.ac.tuwien.engine.util.events.ILEvent;
import at.ac.tuwien.sursystem.model.Datapoint;
import com.google.common.eventbus.Subscribe;
import java.util.ArrayList;
import java.util.List;

/**
 * The Guava Event Bus architecture that allows the local to meta communication.
 *
 * @author Bernhard
 */
public class DatapointListener {

    private Datapoint lastMessage = null;

    private List<Datapoint> queue = new ArrayList<>();
    
    private List<Float> queueIL = new ArrayList<>();
    
    private List<Integer> clusterQueue = new ArrayList<>();
    
    @Subscribe
    public void listenDataPoints(AnonEvent event) {
        lastMessage = event.getMessage();
        queue.add(lastMessage);
    }
    
    @Subscribe
    public void listenInformationLoss(ILEvent event) {
        queueIL.add(event.getInformationloss());
    }

    @Subscribe
    public void listenClusters(CCEvent event) {
        clusterQueue.add(event.getAmountClusters());
    }

    public Datapoint getLastMessage() {
        return lastMessage;
    }
    
    public List<Datapoint> getQueuedDPs(){
        List<Datapoint> empty = new ArrayList<>(queue);
        queue = new ArrayList<>();
        return empty;
    }
    
    public List<Float> getQueuedIL(){
        List<Float> empty = new ArrayList<>(queueIL);
        queueIL = new ArrayList<>();
        return empty;
    }
    
    public Float summarizeQueuedIL(){
        List<Float> empty = new ArrayList<>(queueIL);
        queueIL = new ArrayList<>();
        Float result = 0F;
        for(Float f : empty){
            result = result+f;
        }
        return result;
    }
    
    public Integer getClusterCount() {
        List<Integer> empty = new ArrayList<>(clusterQueue);
        clusterQueue = new ArrayList<>();
        Integer result = 0;
        for (Integer i : empty) {
            result = result + i;
        }
        return result;
    }
}
