/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.analytic;

import at.ac.tuwien.engine.lod.BioPortal;
import at.ac.tuwien.engine.lod.GeoNames;
import java.util.List;
import java.util.Map;

/**
 * Class for testing the hierarchy building process.
 *
 * @author Bernhard
 */
public class TestHierarchy {

    public static void main(String[] args) {
        GeoNames names = new GeoNames();
        boolean test = names.testEndpoint();
        System.out.println("Endpoint: " + test);
        List<String> hierarchy = names.buildHierarchy();
        System.err.println(hierarchy);
        BioPortal portal = new BioPortal();
        List<String> resultSet = portal.buildHierarchy();
        System.err.println(resultSet);
    }
}
