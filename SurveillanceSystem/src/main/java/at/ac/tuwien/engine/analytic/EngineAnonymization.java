/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.analytic;

import at.ac.tuwien.engine.CQELSEngine;
import at.ac.tuwien.engine.util.DatapointListener;
import at.ac.tuwien.sursystem.model.Datapoint;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This is the starting routine for the model.
 *
 * @author Bernhard
 */
public class EngineAnonymization {

    public static void main(String[] args) {
        Model model = ModelFactory.createDefaultModel();
        CQELSEngine engine = new CQELSEngine(model);

        final DatapointListener listener = new DatapointListener();
        at.ac.tuwien.engine.util.EventBus.getInstance().register(listener);

        Timer timer = new Timer("dump", true);
        timer.scheduleAtFixedRate(new TimerTask() {

            /**
             * Dimensional Data will be deleted.
             */
            @Override
            public void run() {
                List<Datapoint> queuedDPs = listener.getQueuedDPs();
                System.out.println("Datapoints: "+queuedDPs);
            }
         }, 10000, 10000);
    }
}
