/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.analytic;

import at.ac.tuwien.datagenerator.GeoDataCache;
import at.ac.tuwien.datagenerator.util.ValueComparator;
import at.ac.tuwien.engine.ClusterService;
import at.ac.tuwien.engine.util.DatapointListener;
import at.ac.tuwien.sursystem.model.Address;
import at.ac.tuwien.sursystem.model.ClinicalCondition;
import at.ac.tuwien.sursystem.model.Datapoint;
import at.ac.tuwien.sursystem.model.Person;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.time.DateUtils;
import org.geojson.LngLatAlt;

/**
 * This class is for running the anonymization approach and tests it behaviour with
 * "real" data. 
 * For evaluation purposes please refer to {@link QIDTest}.
 * @author Bernhard
 */
public class Anonymity {

    public static Map<String, Double> probability = null;

    /**
     * Simple data formatter instance for dates.
     */
    private transient final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

     public static void main(String[] args) {
        ValueComparator bvc = new ValueComparator(GeoDataCache.PROBABILITY);
        probability = new TreeMap<>(bvc);
        probability.putAll(GeoDataCache.PROBABILITY);

        /**
         * K = 2
         * L = 3
         * delta = 10 seconds
         */
        for (int delta = 1; delta < 31; delta++) {
            for (int l = 1; l < 31; l++) {
                for (int k = 1; k < 31; k++) {
                    ClusterService service = new ClusterService(k, l, delta * 250);
                    DatapointListener listener = new DatapointListener();
                    at.ac.tuwien.engine.util.EventBus.getInstance().register(listener);
                    //test anonymization for a whole day
                    Long startTime = System.currentTimeMillis();
                    //Tested until 1600 => 
                    for (int i = 0; i < 1000; i++) {
                        Datapoint dp = generateOne(i);
                        service.classify(dp);
                    }
//            System.out.println("Clusters: "+listener.getClusterCount());
//            System.out.println("InformationLoss: "+listener.summarizeQueuedIL());
//            System.out.println("Time Spent for " + k + " (in msec): " + (System.currentTimeMillis() - startTime));
                    System.out.println(k + "," + l + "," + delta + "," + listener.getClusterCount() + "," + listener.summarizeQueuedIL() + "," + (System.currentTimeMillis() - startTime));
        }
            }
        }
//        System.out.println(listener.getQueuedDPs());
    }

    private static Datapoint generateOne(Integer seed) {
        Datapoint result = new Datapoint();

        ClinicalCondition condition = new ClinicalCondition("Influenza");

        LngLatAlt shape = selectShape();
        Double longD = shape.getLongitude();
        Double latD = shape.getLatitude();
        float lng = longD.floatValue();
        float lat = latD.floatValue();
        Address adr = new Address(lat, lng);

        //build a complete streamed POJO    
        result.setWhere(adr);
        result.setWho(generatePerson());
        Date d = new Date(1000L + seed.longValue());
        result.setWhen(d);
        result.setWhat(condition);

        return result;
    }

    public static LngLatAlt selectShape() {
        //this is the algorithm that chooses a shape
        double rnd = Math.random();
        String selectedShape = null;
        for (Map.Entry<String, Double> entry : probability.entrySet()) {
            String name = entry.getKey();
            Double prob = entry.getValue();
            if (rnd > prob) {
                selectedShape = name;
                break;
            }
        }

        return GeoDataCache.COORDINATE.get(selectedShape);
    }

    public static Person generatePerson() {
        int day = (int) (Math.random() * 365);
        int year = (int) (Math.random() * 50);
        int month = (int) (Math.random() * 12);
        Date d = DateUtils.addDays(new Date(), -day);
        d = DateUtils.addYears(d, -year);
        d = DateUtils.addMonths(d, -month);

        String sex = "Male";
        if (Math.random() > 0.5) {
            sex = "Female";
        }
        Person p = new Person(sdf.format(d), sex);
        return p;
    }
}
