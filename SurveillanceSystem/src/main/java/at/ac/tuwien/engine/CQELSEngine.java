/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine;

import at.ac.tuwien.engine.util.FileChecker;
import at.ac.tuwien.datagenerator.PersonGenerator;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import org.deri.cqels.engine.ContinuousSelect;
import org.deri.cqels.engine.ExecContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the CQELS Engine that continuously generates a stream.
 *
 * @author Bernhard
 */
public class CQELSEngine {

    private final static transient Logger log = LoggerFactory.getLogger(CQELSEngine.class);

    /**
     * The temporary Model of the Engine.
     */
    private ExecContext context = null;

    private Model model;

    public CQELSEngine() {
        model = ModelFactory.createDefaultModel();
        init();
    }

    public CQELSEngine(Model model) {
        this.model = model;
        init();
    }

    /**
     * Creates the temporary CQELS Files in the target-Folder and initalizes
     * CQELS afterwards.
     */
    private void init() {
        String relPath = "C:\\Users\\Bernhard\\Documents\\Diplomarbeit\\repo\\SurveillanceSystem\\";
        File targetDir = new File(relPath + "cqels");
        if (targetDir.exists()) {
            FileChecker check = new FileChecker(targetDir.getAbsolutePath());
            check.deleteFile("cache", ".lck");
            log.info("Target Folder found");
        } else {
            targetDir.mkdir();
            log.info("Target Folder not Found");
        }
        log.info("creating cqles engine...");
        //switch to true for a new initalization
        context = new ExecContext(targetDir.getPath(), false);

        final PersonGenerator gen = new PersonGenerator(model);
        final DefaultRDFStream stream = new DefaultRDFStream(context, "http://ldlab.tuwien.ac.at");

        String query = ""
                + "SELECT "
                + "?sex ?birthday ?phen ?lat ?lng ?startDate \n"
                + " WHERE { STREAM <http://ldlab.tuwien.ac.at> [RANGE 10s SLIDE 5s]{ "
                + "?person <http://www.w3.org/2003/01/geo/wgs84_pos#location> ?point;"
                + "     <http://xmlns.com/foaf/0.1/birthday> ?birthday;"
                + "     <http://xmlns.com/foaf/0.1/gender> ?sex;"
                + "     <http://isis.tuwien.ac.at/surveillance/People#suffers> ?cond."
                + " ?cond <http://www.w3.org/1999/02/22-rdf-syntax-ns#value> ?phen;"
                + "       <http://isis.tuwien.ac.at/surveillance/People#startsAt> ?startDate."
                + " ?point <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2003/01/geo/wgs84_pos#Point>; "
                + "     <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng; "
                + "     <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat. "
                + " }}";

        ContinuousSelect selectAll = context.registerSelect(query);
        CQELSSelectListener listener = new CQELSSelectListener(context);
        selectAll.register(listener);

        /**
         * Data Generation. TODO move to a separate thread.
         */
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                //start with an empty model
                model.removeAll();
                gen.generatePerson();
                stream.stream(model);
            }
        };
        Timer t = new Timer();
        t.scheduleAtFixedRate(task, 0, 100);

    }
}
