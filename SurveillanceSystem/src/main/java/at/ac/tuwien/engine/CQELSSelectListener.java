/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine;

import at.ac.tuwien.sursystem.model.Address;
import at.ac.tuwien.sursystem.model.ClinicalCondition;
import at.ac.tuwien.sursystem.model.Datapoint;
import at.ac.tuwien.sursystem.model.Person;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import org.deri.cqels.data.Mapping;
import org.deri.cqels.engine.ExecContext;
import org.deri.cqels.engine.ContinuousListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Builds the clusters. A cluster is basically a List of occuring attributes.
 * When a cluster is released the list is resetted.
 *
 * @author Bernhard
 */
public class CQELSSelectListener implements ContinuousListener {

    private final ExecContext context;

    private ClusterService clusterService;

    private static final Logger log = LoggerFactory.getLogger(CQELSSelectListener.class);

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    
    public CQELSSelectListener(ExecContext context) {
        this.context = context;
        clusterService = new ClusterService(3, 0, 3000);
    }

    @Override
    public void update(Mapping mapping) {
        try {
            BindingHashMap binding = new BindingHashMap();
            Iterator<Var> iter = mapping.vars();
            Datapoint dp = new Datapoint();
            Double lat = 0.0;
            Double lng = 0.0;
            String phenomenon = null;
            String bDay = null;
            String sex = null;
            String startDate = null;
            while (iter.hasNext()) {
                Var v = iter.next();
                Node decode = context.engine().decode(mapping.get(v));
                log.trace("Node: " + decode);
                Object value = null;
                if (decode.isLiteral()) {
                    value = decode.getLiteralValue();
                } else {
                    value = decode.getLocalName();
                }
                if (v.getName().equals("lat")) {
                    lat = new Double(value.toString());
                }
                if (v.getName().equals("lng")) {
                    lng = new Double(value.toString());
                }
                if (v.getName().equals("phen")) {
                    phenomenon = value.toString();
                }
                if (v.getName().equals("sex")) {
                    sex = value.toString();
                }
                if (v.getName().equals("birthday")) {
                    bDay = value.toString();
                }
                if (v.getName().equals("startDate")) {
                    startDate = value.toString();
                }
                binding.add(v, decode);
            }
            dp.setWhere(new Address(lat.floatValue(), lng.floatValue()));
            dp.setWhat(new ClinicalCondition(phenomenon));
            Date d = sdf.parse(startDate);
            
            dp.setWhen(d);
            dp.setWho(new Person(bDay,sex));
            //classify the CQELS Datapoint
            clusterService.classify(dp);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(CQELSSelectListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
