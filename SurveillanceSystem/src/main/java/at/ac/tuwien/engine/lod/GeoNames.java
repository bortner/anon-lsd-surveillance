/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.lod;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * This is the interface that recieves data from Geonames LOD. This class build
 * the anonymization taxonomy tree for the "Where" Dimension The SPARQL Query
 * for the Endpoint: (e.g. give me all POIs around vienna) <code>
 * Prefix lgdr:<http://linkedgeodata.org/triplify/>
 * Prefix lgdo:<http://linkedgeodata.org/ontology/>
 * Select ?s ?lng ?lat ?street ?city ?district ?country FROM
 * <http://linkedgeodata.org/sparql>
 * where { ?s rdfs:label ?name;
 * <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng;
 * <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;
 * <http://linkedgeodata.org/ontology/addr%3Acity> ?city;
 * <http://linkedgeodata.org/ontology/addr%3Astreet> ?street;
 * <http://linkedgeodata.org/ontology/addr%3Acountry> ?country;
 * <http://linkedgeodata.org/ontology/addr:postcode> ?district
 * FILTER(REGEX(STR(?name), "Wien")) Filter(bif:st_intersects(bif:st_point(?lng,
 * ?lat), bif:st_point (16.366667,48.200001), 10)) }
 * </code>
 *
 * @see <a href="http://linkedgeodata.org/sparql">Endpoint</a>
 * @see <a href="http://www.geonames.org/ontology/ontology_v3.1.rdf">GeoNames
 * Ontology</a>
 *
 * @author Bernhard
 */
public class GeoNames extends SparqlEndpoint {

    /**
     * The query that can copied and pasted into the {@link #url}. <br>
     * Notice:  <br>
     * This query is not executable by jena. As a result we have a hand tailored
     * query especially for jena.
     */
    private String plainSPARQLQuery = "Prefix lgdr: <http://linkedgeodata.org/triplify/> \n"
            + "Prefix lgdo: <http://linkedgeodata.org/ontology/> \n"
            + "Prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
            + "Prefix ld: <http://linkedgeodata.org/ontology/addr%3A>\n"
            + "\n"
            + "Select ?s ?lng ?lat ?street ?city ?district ?country\n"
            + "where {\n"
            + "?s rdfs:label ?name.\n"
            + "?s <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng; \n"
            + "     <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;\n"
            + "     ld:city ?city;\n"
            + "     ld:street ?street;\n"
            + "     ld:country ?country;\n"
            + "     <http://linkedgeodata.org/ontology/addr:postcode> ?district\n"
            + "Filter (bif:st_intersects(bif:st_point(?lng, ?lat), bif:st_point (16.366667,48.200001), 10))\n"
            + "}";

    /**
     * As it is proposed above, this is the special jena query, i.e. all
     * Virtuoso#bifs have been replaced by another construct.
     *
     * Maybe use this query
     * https://groups.google.com/forum/#!topic/linked-geo-data/G_o-v73RXyE
     * http://jena.apache.org/documentation/query/spatial-query.html
     */
    public String jenaQuery = "Prefix lgdr: <http://linkedgeodata.org/triplify/> \n"
            + "Prefix lgdo: <http://linkedgeodata.org/ontology/> \n"
            + "Prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
            + "Prefix ld: <http://linkedgeodata.org/ontology/addr%3A>\n"
            + "Prefix geom: <http://geovocab.org/geometry#>\n"
            + "Prefix ogc: <http://www.opengis.net/ont/geosparql#>\n"
            + "\n"
            + "Select ?s ?lng ?lat ?street ?city ?district ?country\n"
            + "where {\n"
            + "?s rdfs:label ?name.\n"
            + "?s <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng; \n"
            + "     <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;\n"
            + "     ld:city ?city;\n"
            + "     ld:street ?street;\n"
            + "     ld:country ?country;\n"
            + "     <http://linkedgeodata.org/ontology/addr:postcode> ?district;\n"
            + "     geom:geometry ?geom.\n"
            + "  ?geom ogc:asWKT ?geo .\n"
            + "Filter (bif:st_intersects(?geo, bif:st_point ($lng,$lat), 20))\n"
            + "} LIMIT 1";

    /**
     * The query for getting the geo-Hierarchy. Based on the paper of
     *
     * Ngo, Quoc-Hung, Son Doan, and Werner Winiwarter. "Using Wikipedia for
     * extracting hierarchy and building geo-ontology." International Journal of
     * Web Information Systems 8.4 (2012): 401-412.
     *
     */
    private static final String jenaHierarchy = "Prefix lgdr:<http://linkedgeodata.org/triplify/>\n"
            + "Prefix lgdo:<http://linkedgeodata.org/ontology/> \n"
            + "Prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
            + "Prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
            + "Prefix ld: <http://linkedgeodata.org/ontology/addr%3A>\n"
            + "\n"
            + "Select (count(distinct ?llng) as ?lng) (count(Distinct ?cit) as ?city) (count(distinct ?str) as ?street) (count(distinct ?dis) as ?district) (count(distinct ?cou) as ?country) \n"
            + "where {\n"
            + "?s rdfs:label ?name;\n"
            + "    <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?llng; \n"
            + "     <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;\n"
            + "ld:city ?cit;\n"
            + "ld:street ?str;\n"
            + "ld:country ?cou;\n"
            + "<http://linkedgeodata.org/ontology/addr:postcode> ?dis \n"
            + "}";

    /**
     * This query resolves something to geo-Coordinates for displaying it.
     */
    public static final String resolveProperty = "Prefix lgdr: <http://linkedgeodata.org/triplify/> \n"
            + "Prefix lgdo: <http://linkedgeodata.org/ontology/> \n"
            + "Prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
            + "Prefix ld: <http://linkedgeodata.org/ontology/addr%3A>\n"
            + "\n"
            + "Select ?lng ?lat\n"
            + "where {\n"
            + "?s rdfs:label ?name.\n"
            + "{\n"
            + "?s <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng; \n"
            + "	<http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;\n"
            + "	 ld:city ?city.\n"
            + "	 FILTER(?city = \"$search\")\n"
            + "} UNION {\n"
            + "?s <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng; \n"
            + "	<http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;\n"
            + "	ld:street ?street.\n"
            + "	FILTER(?street = \"$search\")\n"
            + "}\n"
            + "UNION {\n"
            + "?s <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng; \n"
            + "	<http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;\n"
            + "	ld:country ?country.\n"
            + "	FILTER(?country = \"$search\")\n"
            + "}\n"
            + "UNION {\n"
            + "?s <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng; \n"
            + "	<http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;\n"
            + "	ld:country ?country.\n"
            + "	FILTER(?country = \"$search\")\n"
            + "}\n"
            + "UNION {\n"
            + "?s <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng; \n"
            + "	<http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat;\n"
            + "	<http://linkedgeodata.org/ontology/addr:postcode> ?district.\n"
            + "	FILTER(?district = \"$search\")\n"
            + "}\n"
            + "} LIMIT 1";

    /**
     * Constructor sets the endpoint.
     */
    public GeoNames() {
//        super.url = "http://linkedgeodata.org/sparql";
        super.url = "http://localhost:8890/sparql";
    }

    @Override
    public Map<String, ?> queryEndpoint(String query) {
        Map<String, Object> results = new HashMap<>();
        QueryExecution qe = new QueryEngineHTTP(super.url, query);
        ResultSet rs = qe.execSelect();
        while (rs.hasNext()) {
            QuerySolution sol = rs.next();
            Iterator<String> varNames = sol.varNames();
            while (varNames.hasNext()) {
                String var = varNames.next();
                RDFNode curNode = sol.get(var);
                if (curNode.isLiteral()) {
                    results.put(var, curNode.asNode().getLiteral().getValue());
                } else {
                    results.put(var, curNode.asResource().getLocalName());
                }
            }
        }
        return results;

    }

    /**
     * Returns the Hierarchy based on the paper of exploitation of strict properties.
     * Based on the paper of Quoc-Hung et al on Page 4 Table I.
     * 
     * @see <a href="http://www.emeraldinsight.com/doi/abs/10.1108/17440081211282892">Paper</a>
     * @return
     */
    @Override
    public List<String> buildHierarchy() {
        Map<String, Object> hierarchy = (Map<String, Object>) super.queryEndpoint(jenaHierarchy);
        NavigableMap<Integer, String> sortedMap = new TreeMap<>();
        for (Map.Entry<String, Object> entry : hierarchy.entrySet()) {
            Integer i = (Integer) entry.getValue();
            sortedMap.put(i, entry.getKey());
        }
        sortedMap = sortedMap.descendingMap();
        Collection<String> values = sortedMap.values();
        return new ArrayList<>(values);
    }

}
