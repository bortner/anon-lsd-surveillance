/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.analytic;

import at.ac.tuwien.engine.util.Pair;
import at.ac.tuwien.engine.util.SetPartition;
import at.ac.tuwien.engine.util.SubsetEnumerator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the example from the paper.
 * <p>
 * Expected is that QIDs with (AGE,SEX) and (AGE,STATE) are returned. The
 * problem is that this algorithm works only within a cluster.
 * The output is a matrix with 
 * TotalQID,State,Sex,Age
 * 
 * @see
 * <a href="http://theory.stanford.edu/~xuying/papers/quasi_vldb1.pdf">Detect
 * QIDs</a>
 * 
 * @author Bernhard
 */
public class QIDTest {

    private final static Logger logger = LoggerFactory.getLogger(QIDTest.class);

    private static Map<Integer, String> statistics = new HashMap<>();

    /**
     * This test tests the amount of generated QID graphic: 
     * J = 20; i = 600 reproduces the result (partly)
     * @param args 
     */
    public static void main(String... args) {
        //the number of iterations
        for (int j = 0; j < 100; j++) {
            //the maximum amount of seeded tuples
            for (int i = 0; i < 100; i++) {
                testRun(i);
            }

            for (Map.Entry<Integer, String> meas : statistics.entrySet()) {
                String value = meas.getValue();
                int sexCount = countOccurences(value, "SEX");
                int ageCount = countOccurences(value, "AGE");
                int stateCount = countOccurences(value, "STATE");
                int totalCount = sexCount + ageCount + stateCount;
                System.out.println(meas.getKey() + "," + totalCount + "," + sexCount + "," + ageCount + "," + stateCount);
            }
            
            statistics = new HashMap<>();
        }
    }

    private static int countOccurences(String str, String pattern) {
        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = str.indexOf(pattern, lastIndex);
            if (lastIndex != -1) {
                count++;
                lastIndex += pattern.length();
            }
        }
        return count;
    }

    /**
     * Runs a single test. 
     * Iteration represents the Number of Data that has to be 
     * (additional to the offset) generated.
     * @param iterations 
     */
    private static void testRun(int iterations) {
        logger.trace("Test-Iteration: " + iterations);
        SubsetEnumerator enumerator = new SubsetEnumerator();
        List<Set<Pair>> subsets = new ArrayList<>();

        logger.trace("AGE Set");
        Set<Pair> subsetAge = enumerator.generateSubset(buildAgeSet(iterations));
        subsets.add(subsetAge);
        logger.trace(Arrays.toString(subsetAge.toArray()));

        logger.trace("SEX Set");
        Set<Pair> subsetSex = enumerator.generateSubset(buildSexSet(iterations));
        subsets.add(subsetSex);
        logger.trace(Arrays.toString(subsetSex.toArray()));

        logger.trace("STATE Set");
        Set<Pair> subsetState = enumerator.generateSubset(buildStateSet(iterations));
        subsets.add(subsetState);
        logger.trace(Arrays.toString(subsetState.toArray()));

        Set<Pair> set = new HashSet<>();
        set.addAll(subsetAge);
        set.addAll(subsetSex);
        set.addAll(subsetState);
        logger.trace("Baseset");
        logger.trace(set.toString());

        logger.trace("Starting Set Partition Problem");

        SetPartition cover = new SetPartition();
        Set<Set<Set<Pair>>> partitions = cover.findCombination(set, subsets);
        if (partitions.isEmpty()) {
            logger.trace("No combinations found!");
//            System.out.println(iterations+ ",");
            String qids = statistics.get(iterations);
            if (qids == null) {
                qids = "";
            }
            statistics.put(iterations, qids);
        }
        for (Set<Set<Pair>> partition : partitions) {
            StringBuilder qid = new StringBuilder();
            qid.append("<");
            for (Set<Pair> par : partition) {
                if (par.containsAll(subsetAge)) {
                    qid.append("AGE ");
                }
                if (par.containsAll(subsetSex)) {
                    qid.append("SEX ");
                }
                if (par.containsAll(subsetState)) {
                    qid.append("STATE");
                }
            }
            qid.append(">");
            logger.trace("iteration: " + iterations + " QID: " + qid.toString());
            StringBuilder qids = new StringBuilder();
            if (statistics.containsKey(iterations)) {
                qids.append(statistics.get(iterations)).append(",");
            }
            qids.append(qid);
            statistics.put(iterations, qids.toString());
//            System.out.println(iterations+ ","+qid.toString());
        }
    }

    private static Map<Integer, Object> buildAgeSet(int iterations) {
        Map<Integer, Object> result = new HashMap<>();
        for (int i = 0; i < iterations; i++) {
            Double d = (Math.random() * 3);
            int selection = d.intValue();
            int age = 0;
            switch (selection) {
                case 0:
                    age = 20;
                    break;
                case 1:
                    age = 30;
                    break;
                default:
                    age = 40;
                    break;
            }
            result.put(i, age);
        }
        result.put(result.size(), 30);
        result.put(result.size(), 20);
        result.put(result.size(), 40);
        logger.trace(result.toString());
        return result;
    }

    private static Map<Integer, Object> buildSexSet(int iterations) {
        Map<Integer, Object> result = new HashMap<>();
        for (int i = 0; i < iterations; i++) {
            Double d = (Math.random() * 2);
            int selection = d.intValue();
            String sex = null;
            switch (selection) {
                case 0:
                    sex = "M";
                    break;
                default:
                    sex = "F";
                    break;
            }
            result.put(i, sex);
        }
        result.put(result.size(), "M");
        result.put(result.size(), "F");
        result.put(result.size(), "F");
        logger.trace(result.toString());
        return result;
    }

    private static Map<Integer, Object> buildStateSet(int iterations) {
        Map<Integer, Object> result = new HashMap<>();
        for (int i = 0; i < iterations; i++) {
            Double d = (Math.random() * 4);
            int selection = d.intValue();
            String state = null;
            switch (selection) {
                case 0:
                    state = "CA";
                    break;
                case 1:
                    state = "TX";
                    break;
                case 2:
                    state = "NY";
                    break;
                default:
                    state = "CA";
                    break;
            }
            result.put(i, state);
        }
        result.put(result.size(), "NV");
        result.put(result.size(), "NV");
        result.put(result.size(), "MS");
        logger.trace(result.toString());
        return result;
    }
}
