/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.engine.lod;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * This is the interface that retrieves data from BioPortal. This class builds
 * the anonymization taxonomy tree for the "What" dimension
 *
 * @see <a href="http://bioportal.bioontology.org/">BioPortal</a>
 * @see <a href="http://sparql.bioontology.org/">Endpoint</a>
 * @author Bernhard
 */
public class BioPortal extends SparqlEndpoint {

    //recursive queryHierarchy: http://answers.semanticweb.com/questions/9909/sparql-recursive-queryHierarchy
    //property Paths not available
    //http://web.stanford.edu/~natalya/papers/iswc2012inuse_bioportal_sparql.pdf
    private static final String key = "a6d4ce07-9108-4872-b28b-59dff8b58bbb";

   private static final String queryHierarchy = " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
            + "     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
            + "     SELECT ?abstract \n"
            + "     WHERE { \n"
            + "     <http://surveillance.mcgill.ca/sso/syndromes.owl#Influenza>  rdfs:subClassOf ?abstract\n"
            + "     }";

    public BioPortal() {
        super.url = "http://sparql.bioontology.org/sparql";
    }

    @Override
    public List<String> buildHierarchy() {
        List<String> hierarchy = new ArrayList<>();
        hierarchy.add("Influenza");
        Query q = QueryFactory.create(queryHierarchy);
        QueryEngineHTTP qexec = QueryExecutionFactory.createServiceRequest(super.url, q);
        qexec.addParam("apikey", this.key);
        ResultSet rs = qexec.execSelect();
        while (rs.hasNext()) {
            QuerySolution soln = rs.nextSolution();
            RDFNode node = soln.get("abstract");
            if (node.isLiteral()) {
                hierarchy.add(node.asNode().getLiteral().getValue().toString());
            } else {
                hierarchy.add(node.asResource().getLocalName());
            }
        }

        return hierarchy;
    }

    @Override
    public Map<String, ?> queryEndpoint(String query) {
        return Collections.emptyMap();
    }

    
    @Override
    public boolean testEndpoint() {
        return true;
    }
}
