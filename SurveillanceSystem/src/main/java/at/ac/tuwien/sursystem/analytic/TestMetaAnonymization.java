/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem.analytic;

import at.ac.tuwien.sursystem.Macroanonymization;
import at.ac.tuwien.sursystem.model.Address;
import at.ac.tuwien.sursystem.model.ClinicalCondition;
import at.ac.tuwien.sursystem.model.Datapoint;
import at.ac.tuwien.sursystem.model.Person;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrBuilder;

/**
 * Test m-Invariance. k-Anonymity is assumed.
 *
 * @author Bernhard
 */
public class TestMetaAnonymization {

    /**
     * The table 1.b from the paper.
     */
    private static List<Datapoint> anonData;
    /**
     * The table 2.b from the paper.
     */
    private static List<Datapoint> anonData2;

    private static Pattern intsOnly = Pattern.compile("\\d+");

    /**
     * This test-case may be tested against ARX Anonymization Framework
     *
     * @see <a href="http://arx.deidentifier.org/">ARX Framework</a>
     * @param args
     */
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            long startTime = System.currentTimeMillis();
//        generateInputData();
            generateInputData(4, 11);
////        generateRealInputData();
            Macroanonymization ma = new Macroanonymization(anonData, anonData2);
            ma.mInvariance();
            //these lines are for long time testing of the meta anonymization algorithm
//          new CQELSEngine();
//        Macroanonymization ma = new Macroanonymization(10000L);
            //outputs counterfeits, total tuples, runtime
            System.out.println("," + (System.currentTimeMillis() - startTime));
        }
    }

    /**
     * Generates input-data as it is generated from WP 2.X - local
     * anonymization.
     */
    public static void generateRealInputData() {
        anonData = new ArrayList<>();
        anonData2 = new ArrayList<>();

        anonData.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));
        anonData.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));
        anonData.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));
        anonData.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));
        anonData.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));
        anonData.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));
        anonData.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));
        anonData.add(generateRealDp(null, 48.1199F, 16.5613F, "Influenza"));
        anonData.add(generateRealDp(null, 48.1199F, 16.5613F, "Influenza"));

        anonData2.add(generateRealDp(null, 48.1199F, 16.5613F, "Influenza"));
        anonData2.add(generateRealDp(null, 48.1199F, 16.5613F, "Influenza"));
        anonData2.add(generateRealDp(null, 48.1199F, 16.5613F, "Influenza"));
        anonData2.add(generateRealDp(null, 48.1199F, 16.5613F, "Influenza"));
        anonData2.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));
        anonData2.add(generateRealDp("2015-02-22", 48.1199F, 16.5613F, "Influenza"));

    }

    /**
     * Generates a k-Anonymized Input table. Usable with datapoints. See table
     * 1.a of the paper.
     */
    public static void generateInputData() {
        anonData = new ArrayList<>();
        anonData2 = new ArrayList<>();
        //Datapoint{where=([12k,14k]), who=[21,22], when=null, what=dyspepsia}
        anonData.add(generateDp("Bob", "[21,22]", "[12k,14k]", "dyspepsia"));
        anonData.add(generateDp("Alice", "[21,22]", "[12k,14k]", "bronchitis"));
        anonData.add(generateDp("Andy", "[23,24]", "[18k,25k]", "flu"));
        anonData.add(generateDp("David", "[23,24]", "[18k,25k]", "gastritis"));
        anonData.add(generateDp("Gary", "[36,41]", "[20k,27k]", "flu"));
        anonData.add(generateDp("Helen", "[36,41]", "[20k,27k]", "gastritis"));
        anonData.add(generateDp("Jane", "[37,43]", "[26k,35k]", "dyspepsia"));
        anonData.add(generateDp("Ken", "[37,43]", "[26k,35k]", "flu"));
        anonData.add(generateDp("Linda", "[37,43]", "[26k,35k]", "gastritis"));
        anonData.add(generateDp("Paul", "[52,56]", "[33k,34k]", "dyspepsia"));
        anonData.add(generateDp("Steve", "[52,56]", "[33k,34k]", "gastritis"));

        anonData2.add(generateDp("Bob", "[21,23]", "[12k,25k]", "dyspepsia"));
        anonData2.add(generateDp("David", "[21,23]", "[12k,25k]", "gastritis"));
        anonData2.add(generateDp("Emily", "[25,43]", "[21k,33k]", "flu"));
        anonData2.add(generateDp("Jane", "[25,43]", "[21k,33k]", "dyspepsia"));
        anonData2.add(generateDp("Linda", "[25,43]", "[21k,33k]", "gastritis"));
        anonData2.add(generateDp("Gary", "[41,46]", "[20k,30k]", "flu"));
        anonData2.add(generateDp("Mary", "[41,46]", "[20k,30k]", "gastritis"));
        anonData2.add(generateDp("Ray", "[54,56]", "[31k,34k]", "dyspepsia"));
        anonData2.add(generateDp("Steve", "[54,56]", "[31k,34k]", "gastritis"));
        anonData2.add(generateDp("Tom", "[60,65]", "[36k,44k]", "gastritis"));
        anonData2.add(generateDp("Vince", "[60,65]", "[36k,44k]", "flu"));

    }

    /**
     * Generates different datapoint like in the paper.
     *
     * @param illness amount of different illness that is seeded
     * @param datapoints amount of generated datapoints
     */
    public static void generateInputData(int illness, int datapoints) {
        anonData = new ArrayList<>();
        anonData2 = new ArrayList<>();
        Map<Integer, String> dict = new HashMap<>();
        dict.put(0, "dyspepsia");
        dict.put(1, "bronchitis");
        dict.put(2, "flu");
        dict.put(3, "gastritis");
        for (int dp = 0; dp < datapoints; dp++) {
            StrBuilder pers = new StrBuilder("Person").append(dp);
            int rnd = (int) (Math.random() * illness);
            String inter = generateInterval(true, null);
            String age = generateInterval(false, null);
            anonData.add(generateDp(pers.toString(), inter, age, dict.get(rnd)));
            if (Math.random() > 0.8) {
                rnd = (int) (Math.random() * illness);
                anonData.add(generateDp(pers.toString(), inter, age, dict.get(rnd)));
            }
            //mock the second view, seed it from the first
            rnd = (int) (Math.random() * illness);
            String inter2 = generateInterval(true, inter);
            String age2 = generateInterval(false, age);
            anonData2.add(generateDp(pers.toString(), inter2, age2, dict.get(rnd)));
            if (Math.random() > 0.8) {
                rnd = (int) (Math.random() * illness);
                anonData2.add(generateDp(pers.toString(), inter2, age2, dict.get(rnd)));
            }
        }
//        System.out.println(Arrays.toString(anonData.toArray()));
//        System.out.println(Arrays.toString(anonData2.toArray()));
    }

    public static String generateInterval(boolean age, String seed) {
        List<Integer> seeds = getSeeds(seed);
        StrBuilder sb = new StrBuilder();
        sb.append("[");
        if (seeds.isEmpty()) {
            if (age) {
                int fAge = 10 + (int) (Math.random() * 50);
                int tAge = (int) (20 + Math.random() * 50);
                if (fAge > tAge) {
                    int tmp = tAge;
                    tAge = fAge;
                    fAge = tmp;
                }
                sb.append(fAge);
                sb.append(",");
                sb.append(tAge);
            } else {
                int fZIP = (int) (Math.random() * 10);
                int tZIP = (int) (10 + Math.random() * 10);
                if (fZIP > tZIP) {
                    int tmp = tZIP;
                    tZIP = fZIP;
                    fZIP = tmp;
                }
                sb.append(fZIP).append("k");
                sb.append(",");
                sb.append(tZIP).append("k");
            }
        } else {
            if (age) {
                int fAge = seeds.get(0);
                fAge = fAge - (int) (Math.random() * 3);
                int tAge = seeds.get(1);
                tAge = tAge + (int) (Math.random() * 3);
                sb.append(fAge);
                sb.append(",");
                sb.append(tAge);
            } else {
                int fZIP = seeds.get(0);
                fZIP = fZIP - (int) (Math.random() * 3);
                int tZIP = seeds.get(1);
                tZIP = tZIP + (int) (Math.random() * 3);
                sb.append(fZIP).append("k");
                sb.append(",");
                sb.append(tZIP).append("k");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Assistence method for generating a Datapoint.
     *
     * @param name
     * @param anonAge
     * @param anonZip
     * @param clinicalCond
     * @return
     */
    public static Datapoint generateDp(String name, String anonAge, String anonZip, String clinicalCond) {
        Datapoint dp = new Datapoint();
        Person p = new Person();
        p.setName(name);
        //age tweak
        p.setBirthDate(anonAge);
        Address a = new Address();
        a.setDistrict(anonZip);
        ClinicalCondition cc = new ClinicalCondition(clinicalCond);
        dp.setWho(p);
        dp.setWhat(cc);
        dp.setWhere(a);
        return dp;
    }

    /**
     * Same for real data.
     *
     * @param dob
     * @param lat
     * @param lng
     * @param clinicalCond
     * @return
     */
    public static Datapoint generateRealDp(String dob, float lat, float lng, String clinicalCond) {
        Datapoint dp = new Datapoint();
        Person p = new Person();
        p.setBirthDate(dob);
        Address a = new Address(lat, lng);
        ClinicalCondition cc = new ClinicalCondition(clinicalCond);
        dp.setWho(p);
        dp.setWhat(cc);
        dp.setWhere(a);
        return dp;
    }

    public static List<Integer> getSeeds(String str) {
        List<Integer> result = new ArrayList<>();
        if (StringUtils.isNotEmpty(str)) {
            Matcher makeMatch = intsOnly.matcher(str);
            while (makeMatch.find()) {
                result.add(Integer.valueOf(makeMatch.group()));
            }
        }
        return result;
    }

}
