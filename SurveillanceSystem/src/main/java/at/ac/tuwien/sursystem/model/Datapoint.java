/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem.model;

import java.util.Date;

/**
 * This is a single datapoint. A datapoint represents an Entry consisting of the 
 * four dimension (When, What, Where, Who). This four dimension have to be annotated by hand.
 *
 * @author Bernhard
 */
public class Datapoint {

    //where - dimension
    public Address where;

    //who dimension
    public Person who;

    //when dimension
    public Date when;

    //what
    public ClinicalCondition what;

    public void setWhere(Address where) {
        this.where = where;
    }

    public Address getWhere() {
        return this.where;
    }

    public Person getWho() {
        return this.who;
    }

    public void setWho(Person person) {
        this.who = person;
    }

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date timeInMillis) {
        this.when = timeInMillis;
    }

    public ClinicalCondition getWhat() {
        return what;
    }

    public void setWhat(ClinicalCondition what) {
        this.what = what;
    }

    @Override
    public String toString() {
        return "Datapoint{" + "where=" + where + ", who=" + who + ", when=" + when + ", what=" + what + '}';
    }


}
