/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem.model;

import at.ac.tuwien.engine.ClusterService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a sub-Cluster. A Sub-Cluster is a data vector with additional anonymization 
 * attributes. Furthermore it has to contain the rawData and the anonymized data for
 * calculating the ILM ratio. Generalization is ensured by setting an appropriate 
 * {@link SubCluster#anonLevel}. This level is the position of the attribute of the 
 * hierarchy of the taxonomy tree. Therefore to generalize this cluster an appropriate
 * Tree has to be build before {@link ClusterService}. Finally {@link SubCluster#suppress}
 * indicates if the Sub-Cluster is marked as suppressed cluster, i.e. it is aready merged
 * because it does not fulfill the k-Anonymity constraints. <p>
 * 
 *Notice: Suppression always affects the anonymized Data, the raw-data is always returned.
 * 
 * @author Bernhard
 */
public class SubCluster<T> {

    /**
     * The property that sets the Anonymization Level for the whole cluster.
     */
    public static final String GLOBAL_ANONYMIZATION = "global";
    /**
     * level of anonymization in this cluster w.r.t. a single attribute.
     * all means that the whole cluster is anonymized, whereas a single attribute is
     * mapped as "Attribute-Name" to level
     * 0.. raw 
     * 1.. first level ..
     * 
     */
    private Map<String, Integer> anonLevel = new HashMap<>();

    /**
     * The Raw Data for this SubCluster. Used for calculating the ILM Metric
     */
    private List<T> rawData = new ArrayList<>();

    /**
     * The anonymized Data for this subCluster. This is naturally used.
     */
    private List<T> anonData = new ArrayList<>();

    /**
     * Indicate if a cluster is suppressed, because it is merged.
     * A merged cluster can be un-suppressed by the next iteration, if there is
     * enough data to fulfill k-Anonymity. See Sweneey paper for additional Information.
     */
    public boolean suppress = false;

    /**
     * Desired constructor. Initalize the "all" Attribute on Level Zero
     */
    public SubCluster() {
        anonLevel.put(GLOBAL_ANONYMIZATION, 0);
        
    }
    /**
     * Gets the anonymization level within a specific attribute.
     * All is a special key that sets the level of the whole cluster.
     * 
     * @param attribute
     * @return 
     */
    public Integer getAnonLevel(String attribute) {
        return anonLevel.get(attribute);
    }

    /**
     * Sets the level of anonymization 
     * @param attribute
     * @param anonLevel 
     */
    public void setAnonLevel(String attribute,int anonLevel) {
        this.anonLevel.put(attribute, anonLevel);
    }

    public void addAnonLevels(Map<String, Integer> hierarchy){
        this.anonLevel.putAll(hierarchy);
    }
    
    public Map<String, Integer> getAnonLevels(){
        return this.anonLevel;
    }
    
    public List<T> getRawData() {
        return rawData;
    }

    public void addRawData(T rawData) {
        this.rawData.add(rawData);
    }

    public List<T> getAnonData() {
        if (!suppress) {
            return anonData;
        } else {
            return Collections.emptyList();
        }
    }
    
    public void setAnonData(List<T> anonData){
        this.anonData = anonData;
    }

    public void addAnonData(T anonData) {
        this.anonData.add(anonData);
    }

    public int size() {
        return this.anonData.size();
    }
}
