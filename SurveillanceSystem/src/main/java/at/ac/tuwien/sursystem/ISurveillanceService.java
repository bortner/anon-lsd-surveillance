/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem;

import com.pusher.rest.Pusher;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * The interface that is called by {@link Pusher}. To recieve Pusher events the
 * Frontend has to call {@link #registerChannel(java.lang.String)}. For
 * demonstration purposes the backend has the ability to get a deeper insight
 * into the clusters by various stateless calls.
 *
 * @see #getCluster(java.lang.String) returns a cluster given by an ID
 * @see #getClusterList() return all anonymized clusters
 * @see #getClusterILM(java.lang.String) gets the timeseries of ILM by a cluster
 * Id
 * @author Bernhard
 */
@Path("/surveillance")
@Produces(MediaType.APPLICATION_JSON)
public interface ISurveillanceService {

    /**
     * Registers frontend callers to the pusher instance.
     *
     * @param channel
     * @return
     */
    @GET
    @Path("/{channel}")
    public Response registerChannel(@PathParam("channel") String channel);

    /**
     * gets a list of all available Clusters.
     *
     * @return
     */
    @GET
    @Path("/cluster/list")
    public Response getClusterList();

    /**
     * gets a cluster.
     *
     * @param id
     * @return
     */
    @GET
    @Path("/cluster/{id}")
    public Response getCluster(@PathParam("id") String id);

    /**
     * gets the information loss metric of a cluster.
     *
     * @param id
     * @return
     */
    @GET
    @Path("/cluster/{id}/ilm")
    public Response getClusterILM(@PathParam("id") String id);

}
