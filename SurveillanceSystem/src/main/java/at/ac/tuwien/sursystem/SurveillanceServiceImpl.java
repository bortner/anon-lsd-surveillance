/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem;

import at.ac.tuwien.engine.CQELSEngine;
import at.ac.tuwien.sursystem.model.Cluster;
import at.ac.tuwien.sursystem.model.SurvPusher;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Publish / Subscribe (PubSub) Backend for Meta-Anonymization. Due to the
 * limitations of {@link SurvPusher} the maximum amount of the sandbox (100k
 * messages and 20 channels). A channel can be manually set up by calling
 * {@link http://localhost:8080/SurveillanceSystem/services/example/{channel-id}}
 *
 * @see <a href="http://pusher.com/docs/libraries">SurvPusher Libraries</a>
 * @see <a href="https://github.com/pusher/pusher-rest-java">SurvPusher
 * Backend</a>
 *
 * @author Bernhard
 */
@Service("surveillanceService")
public class SurveillanceServiceImpl implements ISurveillanceService {

    private static final transient Logger logger = LoggerFactory.getLogger(SurveillanceServiceImpl.class);

    public SurveillanceServiceImpl() {
        //start CQELSEngine
        new CQELSEngine();
        //start Meta-Anonymization
        Macroanonymization ma = new Macroanonymization(10000L);
        
    }
    
   

    /**
     * The List of clusters.
     */
    private static final Map<String, Cluster> clusters = new HashMap<>();

    private static List<String> channels = new ArrayList<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public Response registerChannel(final String channel) {
        logger.info("Registered {} on pusher", channel);
        channels.add(channel);
        return Response.ok().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response getClusterList() {
        logger.info("getting List of all available Clusters");
        String[] clusterArray = clusters.keySet().toArray(new String[clusters.size()]);
        return Response.ok(clusterArray).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response getCluster(String id) {
        logger.info("returning cluster with id {}" + id);
        Cluster cluster = clusters.get(id);
        return Response.ok(cluster).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response getClusterILM(String id) {
        Cluster cluster = clusters.get(id);
        Float ilm = cluster.getInformationLoss();
        return Response.ok(ilm).build();
    }

    public static List<String> getChannels() {
        return SurveillanceServiceImpl.channels;
    }

    public static void push() {
        System.out.println(Arrays.toString(channels.toArray()));
    }
}
