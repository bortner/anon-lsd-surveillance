/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem.model;

import at.ac.tuwien.engine.ClusterService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

/**
 * Datamodel for a single anonymized Cluster. A cluster is a bag of a Property
 * of a datapoints, e.g. a where cluster contains all addresses of the
 * datapoint. Within a cluster there is a level of anonymization
 * {@link Cluster#anonLevel} indicating the current anonymization level.
 *
 * @author Bernhard
 */
@XmlRootElement(name = "Cluster")
public class Cluster<T> {

    private final static Logger logger = LoggerFactory.getLogger(Cluster.class);

    /**
     * The subclusters of this cluster. A subcluster is a cluster that has the
     * same Object value. E.g. Vienna is one SubCluster, and Graz.
     */
    private Map<String, SubCluster<T>> subClusters;

    /**
     * Implicitly calculated Information Loss metric.
     *
     * @see ILM
     */
//    private final ILM ilm;
    /**
     * The hierarchy that should be applied by generalization.
     */
    private List<String> hierarchy;
    /**
     * K-Anonymity. Each SubCluster has to have at least k- Elements. Otherwise
     * it will be merged with other Constraint Violating Subclusters. Merging a
     * Subcluster implies that the subcluster is suppressed for this
     * iteration,i.e. it is not analytical processed. By default we have
     * 2-Anonymity
     */
    int k = 2;

    /**
     * L-Diversity. Each SubCluster has to have l "well distributed" Attributes.
     * In this case distinct l-diversity is implemented,i.e. that the sensitive
     * attribute has at least l occurences.
     */
    int l = 2;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Desired Constructur. Requires a Generalization hierarchy.
     *
     * @param hierarchy
     */
    public Cluster(List<String> hierarchy, int k, int l) {
        UUID id = UUID.randomUUID();
//        ilm = new ILM(id.toString());
        subClusters = new HashMap<>();
        this.hierarchy = hierarchy;
        this.k = k;
        this.l = l;
    }

    /**
     * Generalizes a given SubCluster. ##TODO there is a key issue
     *
     * @param key
     */
    public synchronized void generalizeSubCluster(String key) {
        SubCluster sc = subClusters.get(key);
        if (sc == null) {
            logger.trace("Cluster {} has been released! ", key);
            return;
        }
        logger.trace("Generalizing {}", key);
        if (!this.hierarchy.isEmpty()) {
            int anonClusterLevel = sc.getAnonLevel(SubCluster.GLOBAL_ANONYMIZATION);
            if (!testKAnonymity(k, key)) {
                anonClusterLevel = anonClusterLevel + 1;
            } else {
                anonClusterLevel = anonClusterLevel - 1;
            }
            if ((0 < anonClusterLevel) && (anonClusterLevel < this.hierarchy.size())) {
                logger.trace("Dimension{} -> AnonLevel {}", key, anonClusterLevel);
            } else {
                if (anonClusterLevel >= this.hierarchy.size()) {
                    anonClusterLevel = this.hierarchy.size() - 1;
                } else {
                    anonClusterLevel = 0;
                }
            }
            sc.setAnonLevel(SubCluster.GLOBAL_ANONYMIZATION, anonClusterLevel);
            sc.getAnonData().clear();
            //here we have a problem when using the raw value of (lat, lng) 
            //in this special case we just generate lng as key which is invalid...
            logger.debug("Hierarchy Level: {}", anonClusterLevel);
            for (Object valObj : sc.getRawData()) {
                T value = (T) valObj;
                int curLevel = this.hierarchy.indexOf(value);
                if (!this.hierarchy.isEmpty()) {
                    if (anonClusterLevel == 0) {
                        logger.trace("Raw Release of {}", value);
                        sc.addAnonData(value);
                    } else if (curLevel != anonClusterLevel) {
                        String newDimension = this.hierarchy.get(anonClusterLevel);
                        T newValue = (T) getValue(value, newDimension);
                        logger.trace(value + " has been generalized by " + newValue);
                        sc.addAnonData(newValue);
                    }
                }
            }
        } else {
            List<String> keys = new ArrayList<>(subClusters.keySet());
            for (String curkey : keys) {
                boolean sameKey = false;
                SubCluster csc = subClusters.get(curkey);
                if (testKAnonymity(k, curkey)) {
                    csc.suppress = false;
                    //delete the anonymization mapping
                    csc.setAnonData(new ArrayList<>());
                    //if it is k anonym try to go one step lower
                    List<T> data = csc.getRawData();
                    //move one step lower
                    int curAnonLevel = csc.getAnonLevel(SubCluster.GLOBAL_ANONYMIZATION) - 1;
                    //rebalance the rest.
                    //new Clusters proposal
                    Map<String, SubCluster<T>> newClusters = new HashMap<>();
                    for (T dp : data) {
                        String newValue = dp.toString();
                        String right = StringUtils.right(newValue, curAnonLevel);
                        String left = StringUtils.left(newValue, (newValue.length() - curAnonLevel));
                        StrBuilder sb = new StrBuilder(left).append(right.replaceAll(".", "*"));
                        newValue = sb.toString();
                        //wenns keinen proposal gibt füge ihn hinzu
                        if (!newClusters.containsKey(newValue)) {
                            SubCluster<T> newCluster = new SubCluster<T>();
                            newCluster.setAnonLevel(SubCluster.GLOBAL_ANONYMIZATION, curAnonLevel);
                            newClusters.put(newValue, newCluster);
                        }
                        //sortiere 
                        bucketSort(dp, newClusters);
                    }
                    for (Map.Entry<String, SubCluster<T>> entry : newClusters.entrySet()) {
                        //splitting in a subcluster has succeed. anonymize it
                        List<T> rawData = entry.getValue().getRawData();
                        if (rawData.size() > (k - 1)) {
                            subClusters.put(entry.getKey(), entry.getValue());
                            if (key.equals(entry.getKey())) {
                                sameKey = true;
                            }
                            //remove the old data, it is moved to the shiny new subcluster
                            for (T dp : rawData) {
                                replaceValue(dp, entry.getKey(), key);
                                csc.addAnonData(dp);
                            }
                        } else {
                            for (T dp : rawData) {
                                csc.addRawData(dp);
                            }
                        }
                        if(testKAnonymity(k, key)){
                            csc.suppress = false;
                        }
                        //remove current subcluster if it has been split
                        if (csc.getRawData().isEmpty() && !sameKey) {
                            subClusters.remove(curkey);
                        }
                    }
                } else {
                    csc.suppress = true;
                }
            }
        }
    }

    /**
     * Adds a subCluster to the cluster. Used for Merge-Cluster operations.
     *
     * @param key
     * @param cl
     */
    public void addSubCluster(String key, SubCluster cl) {
        subClusters.put(key, cl);
    }

    /**
     * Adds a dimension to the data Vector. If a vector does not exists, a new
     * SubCluster is generated.
     *
     * @param dimension
     * @return String key that has been generated
     */
    public synchronized String addDataVector(T oldDimension) {
        T dimension = generateInstance(oldDimension);
        String key = null;
        if (!(oldDimension instanceof Date)) {
            key = oldDimension.toString();
        } else {
            key = sdf.format(oldDimension);
        }
        SubCluster sc = subClusters.get(key);
        if (sc == null) {
            sc = new SubCluster();
        }

        if (this.hierarchy.isEmpty()) {
            sc.suppress = true;
            List<String> fields = getFields(oldDimension);
            int steps = 0;
            boolean noCluster = false;
            //we assume that its a primitive -> primitives have no public fields
            if (fields.isEmpty()) {
                sc.addRawData(key);
                subClusters.put(key, sc);

            } else {
                for (String field : fields) {
                    Object oldValue = getValue(oldDimension, field);
                    String newValue = oldValue.toString();

//                 if (oldValue instanceof Number) {
//                 Integer n = (Integer) oldValue;
//                 newValue = generalizeInterval(n, 10);
//                 } else
                    if (ClusterService.testDate(newValue)) {
                        String nearestKey = findNearestCluster(newValue);
                        if (nearestKey != null) {
                            sc = subClusters.get(nearestKey);
                            steps = sc.getAnonLevel(SubCluster.GLOBAL_ANONYMIZATION);
                            int stepx = getFirstDifferentSight(nearestKey, newValue);
                            if((nearestKey.length() - steps) >= stepx){  
                                steps = steps - 1;
                                
                            } else {
                                steps = steps+1;
                            }
                            sc.setAnonLevel(SubCluster.GLOBAL_ANONYMIZATION, steps);
                        } else {
                            steps = newValue.length() - 1;
                            sc.setAnonLevel(SubCluster.GLOBAL_ANONYMIZATION, steps);
                            noCluster = true;
                        }

                        //return the right signs of the string
                        String right = StringUtils.right(newValue, steps);
                        String left = StringUtils.left(newValue, (newValue.length() - steps));
                        StrBuilder sb = new StrBuilder(left).append(right.replaceAll(".", "*"));
                        newValue = sb.toString();
                    }

                    logger.trace(oldValue + " has been generalized by " + newValue);
                    setField(dimension, field, newValue);
                }
                key = dimension.toString();
                if (noCluster) {
                    subClusters.put(key, sc);
                }
                sc.addAnonData(dimension);
                key = bucketSort(dimension, subClusters);
            }
        } else {
            sc.addRawData(oldDimension);
            subClusters.put(key, sc);
        }

        //test if the new cluster fulfills k-Anonymity
//        if (testKAnonymity(k, key)) {
        if (sc.getRawData().size() > (k - 1)) {
            sc.suppress = false;
        } else {
            sc.suppress = true;
        }

        return key;
    }

    /**
     * Gets a list of clusters that are potentitally candidates for merging.
     * When a candidate is selected, the appropriate subcluster is suppressed.
     *
     * @return
     */
    public synchronized List<SubCluster> getMergeCandidates() {
        List<SubCluster> candidates = new ArrayList<>();
        Map<String, SubCluster<T>> copy = new HashMap<>(subClusters);
        copy.remove("Merged");
        for (Map.Entry<String, SubCluster<T>> entry : copy.entrySet()) {
            SubCluster sc = entry.getValue();
            if (sc.getRawData().size() < k) {
                logger.trace("Found a Candidate {}", sc.getRawData());
                candidates.add(sc);
                sc.suppress = true;
            } else {
                sc.suppress = false;
            }
        }
        return candidates;
    }

    /**
     * Builds the indexed Property List, that is used for determining the QID
     * Combinations.
     *
     * @return
     */
    public synchronized Map<Integer, T> getIndexedProperties() {
        Map<Integer, T> result = new HashMap<>();
        int i = 0;
        for (Map.Entry<String, SubCluster<T>> map : subClusters.entrySet()) {
            SubCluster scl = map.getValue();
            if (!scl.suppress && testLDiversity(l)) {
                List<T> data = scl.getAnonData();
                for (Object anon : data) {
                    result.put(i, (T) anon);
                    i++;
                }
            } else {
                logger.trace("L-Diversity Fail");
            }
        }
        return result;
    }

    /**
     * releases the Cluster, i.e. deletes all contained information.
     */
    public synchronized Integer releaseCluster() {
        logger.trace("Removing all cached data");
        Integer cnt = this.subClusters.size();
        this.subClusters = new HashMap<>();
        return cnt;
    }

    /*
     * Dont include the ILM by default. There is a separate call for that.
     */
    @JsonIgnore
    public synchronized Float getInformationLoss() {
        Float infLoss = 0F;
        try {
            List<SubCluster<T>> scl = new ArrayList<>(subClusters.values());
            for (SubCluster sc : scl) {
                logger.trace("SubCluster: " + sc.getRawData());
                //the sum hierarchy of the second polynome
                Map<String, Integer> informationLoss = sc.getAnonLevels();
                //there are empty clusters !!!! 
                if (sc.getRawData().isEmpty()) {
                    //we dont have to remove them, because they are thrown away in the
                    //next iteration.
                    return 0F;
                }
                Float tIL = 0F;
                if (!sc.suppress) {

                    int hierSize = (hierarchy.size() != 0) ? hierarchy.size() : 1;
                    for (Map.Entry<String, Integer> ile : informationLoss.entrySet()) {

                        Float il = new Float(ile.getValue());
                        //the fraction for this value
                        float div = (il / hierSize);
                        logger.trace("IL \t " + ile.getKey() + " " + div);
                        tIL = tIL + div;
                    }
                } else {
                    //its suppressed, implies that we have max(sc.getHierarchy) as ILM
                    logger.trace("Its suppressed -> adding " + this.hierarchy.size());
                    tIL = new Float(hierarchy.size());
                }
                //the sum fraction for all clusters
                infLoss = infLoss + tIL * informationLoss.size();
            }
        } catch (Throwable t) {
            logger.warn(t.getMessage(), t);
        }
        return infLoss;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        String cluster = gson.toJson(this);
        return cluster;
    }

    /**
     * Attempt to find the nearest cluster. The distance between the keys has to
     * be at least |key|.
     *
     * @param id
     * @return
     */
    protected String findNearestCluster(String id) {
        Set<String> clusterKeys = subClusters.keySet();
        String choosenKey = null;
        int nearest = id.length();
        for (String curKey : clusterKeys) {
            int rate = StringUtils.getLevenshteinDistance(curKey, id);
            if (rate < nearest) {
                nearest = rate;
                choosenKey = curKey;
            }
        }
        return choosenKey;
    }

    /**
     * Generalize a given value to an interval. The interval is left Opened,
     * i.e. 25 with a border of 5 will result in 25 - 30. Maybe interesting for
     * ZIP Codes or Ages.
     *
     * @param number to be generalized
     * @param border to which interval
     * @return generalized interval
     */
    protected String generalizeInterval(Integer number, Integer border) {
        int lower = (int) (Math.floor(number.floatValue() / border)) * border;
        int upper = (int) (Math.ceil(number.floatValue() / border)) * border;
        if (lower == upper) {
            upper = upper + border;
        }
        return lower + " - " + upper;
    }

    /**
     * Generates a new Instance of the given Object.
     *
     * @param object
     * @return
     */
    protected T generateInstance(T object) {
        T instance = null;
        try {
            instance = (T) object.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
        }
        return instance;
    }

    /**
     * Gets a value from the specific field of the specified object.
     *
     * @param dp
     * @param field
     * @return
     */
    protected Object getValue(T dp, String field) {
        try {
            Class<?> clazz = dp.getClass();
            Field findField = ReflectionUtils.findField(clazz, field);
            if (findField == null) {
                return field;
            }
            Object value = findField.get(dp);
            return value;
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            logger.trace(ex.getMessage(), ex);
        }
        return null;
    }

    protected void setField(T dp, String field, Object value) {
        Field f = ReflectionUtils.findField(dp.getClass(), field);
        ReflectionUtils.setField(f, dp, value);
    }
    
    protected void replaceValue(T dp, String oldValue, String newValue){
        List<String> fields = getFields(dp);
        for(String field : fields){
             Field f = ReflectionUtils.findField(dp.getClass(), field);
             Object value = ReflectionUtils.getField(f, dp);
             if(value.toString().equals(oldValue)){
                 setField(dp, field, newValue);
                 return;
             }
        }
    }

    /**
     * get fields from a single Dimension.
     *
     * @param dp
     * @return
     */
    protected List<String> getFields(T dp) {
        final List<String> fields = new ArrayList<>();
        Class<?> clazz = dp.getClass();
        ReflectionUtils.doWithFields(clazz, new ReflectionUtils.FieldCallback() {

            @Override
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                if (!Modifier.isPrivate(field.getModifiers())) {
                    fields.add(field.getName());
                }
            }
        });
        return fields;
    }

    /**
     * A cluster is k-anonym iff it contains k-1 data.
     *
     * @param k
     * @param key
     */
    private boolean testKAnonymity(int k, String key) {
        k = k - 1;
        SubCluster sc = subClusters.get(key);
        if (sc.getRawData().size() > k) {
            return true;
        }

        return false;
    }

    /**
     * Tests if a sensitive Attribue differs at least l times. (Distinct
     * l-diversity)
     *
     * @param l
     * @param key
     * @return
     */
    private boolean testLDiversity(int l) {
        int cnt = subClusters.size();
        if (cnt > l) {
            return true;
        }
        return false;
    }

    /**
     * Naja prinzipiell ist es so, dass jeder bucket k werte haben muss.
     *
     * ## TODO THINK ABOUT A LOWER BOUND
     *
     * @param dp
     */
    public String bucketSort(T dp, Map<String, SubCluster<T>> buckets) {
        String string = dp.toString();
        int curRatio = Integer.MAX_VALUE;
        String similarKey = null;
        //detect most similar bucket
        for (Map.Entry<String, SubCluster<T>> bucket : buckets.entrySet()) {
            String key = bucket.getKey();
//            int ratio = LevenshteinDistance(key, string);
            int ratio = StringUtils.getLevenshteinDistance(key, string);
            if (ratio < curRatio) {
                curRatio = ratio;
                similarKey = key;
            }
        }
        if (similarKey != null && !similarKey.isEmpty()) {
            SubCluster<T> curBucket = buckets.get(similarKey);
            curBucket.addRawData(dp);
        }
        return similarKey;
    }

    protected int getFirstDifferentSight(String reference, String compare){
        int dif = 0;
        char[] ref = reference.toCharArray();
        char[] comp = compare.toCharArray();
        //we start from left: 2012-01-01
        for(int i = 0; i < ref.length-1; i++){
            char a = ref[i];
            char b = comp[i];
            if(a == b){
                dif++;
            } else {
                break;
            }
            
        }
        return dif;
    }
}
