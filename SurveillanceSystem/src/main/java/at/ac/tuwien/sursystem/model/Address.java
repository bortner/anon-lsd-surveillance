/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem.model;

/**
 * This is the Address DTO. It is a lightweight implementation of the {@link at.ac.tuwien.datagenerator.ontology.Point}
 * @author Bernhard
 */
public class Address {

    String district;

    String street;

    String city;

    String country;

    Float lat;

    Float lng;

    public Address() {
    }

    public Address(Float lat, Float lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public Address(String district, String street, String country, Float lat, Float lng, String city) {
        this.district = district;
        this.street = street;
        this.country = country;
        this.lat = lat;
        this.lng = lng;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        if (lat != null && lng != null) {
            return "(" + lat + ", " + lng + ")"; //To change body of generated methods, choose Tools | Templates.
        } else if (street != null) {
            return "(" + street + ")";
        } else if (city != null) {
            return "(" + city + ")";
        } else if (district != null) {
            return "(" + district + ")";
        } else {
            return "(" + country + ")";
        }
    }

}
