/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem.model;

import at.ac.tuwien.engine.ClusterService;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Bernhard
 */
@Ignore
public class ClusterTest {

    private transient final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public ClusterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFindAllFields() {
        Cluster instance = new Cluster(Collections.emptyList(), 2, 3);
        Person p = new Person(new Date().toString(), "lala");
        List fields = instance.getFields(p);
        assertEquals("sex", fields.get(1));
        assertEquals("birthDate", fields.get(0));
    }

    /**
     * Test of generalizeInterval method, of class Cluster.
     */
    @Test
    public void testGeneralizeInterval() {
        System.out.println("generalizeInterval");
        Cluster instance = new Cluster(Collections.emptyList(), 2, 3);
        String result = instance.generalizeInterval(22, 5);
        String expResult = "20 - 25";
        assertEquals(expResult, result);
        result = instance.generalizeInterval(25, 5);
        expResult = "25 - 30";
        assertEquals(expResult, result);
        result = instance.generalizeInterval(27, 5);
        expResult = "25 - 30";
        assertEquals(expResult, result);
        result = instance.generalizeInterval(30, 5);
        expResult = "30 - 35";
        assertEquals(expResult, result);
    }

    @Test
    public void testRegex() {
        String match = "2014-01-01";
        boolean positive = ClusterService.testDate(match);
        assertTrue(positive);
//        match = new Date().toString();
//        positive = ClusterService.testDate(match);
//        assertTrue(positive);
        String dismatch = "ABC";
        boolean negative = ClusterService.testDate(dismatch);
        assertFalse(negative);
    }

    @Test
    public void testBucketSort() {
        Map<String, SubCluster<String>> buckets = new HashMap<>();
        SubCluster<String> cluster1 = new SubCluster<>();
        SubCluster<String> cluster2 = new SubCluster<>();
        SubCluster<String> cluster3 = new SubCluster<>();
        buckets.put("111", cluster1);
        buckets.put("222", cluster2);
        buckets.put("333", cluster3);
        Cluster instance = new Cluster(Collections.emptyList(), 2, 3);
        instance.bucketSort("112", buckets);
        assertEquals("112", cluster1.getRawData().get(0));

        instance.bucketSort("122", buckets);
        assertEquals("122", cluster2.getRawData().get(0));

    }

    @Test
    public void testComplexBucketSort() {
        Map<String, SubCluster<String>> buckets = new HashMap<>();
        SubCluster<String> cluster1 = new SubCluster<>();
        SubCluster<String> cluster2 = new SubCluster<>();
        buckets.put("19********", cluster1);
        buckets.put("20********", cluster2);
        Cluster instance = new Cluster(Collections.emptyList(), 2, 3);

        instance.bucketSort("1970-01-01", buckets);
        instance.bucketSort("1971-12-12", buckets);
        assertEquals("1970-01-01", cluster1.getRawData().get(0));
        assertEquals("1971-12-12", cluster1.getRawData().get(1));

        instance.bucketSort("2001-01-01", buckets);
        assertEquals("2001-01-01", cluster2.getRawData().get(0));
    }
    
    @Test
    public void testComplexBucketSort2() {
        Map<String, SubCluster<String>> buckets = new HashMap<>();
        SubCluster<String> cluster1 = new SubCluster<>();
        SubCluster<String> cluster2 = new SubCluster<>();
        buckets.put("Male", cluster1);
        buckets.put("Female", cluster2);
        Cluster instance = new Cluster(Collections.emptyList(), 2, 3);

        instance.bucketSort("Male", buckets);
        instance.bucketSort("Female", buckets);
        instance.bucketSort("Male", buckets);
        assertEquals(2, cluster1.getRawData().size());
        assertEquals(1, cluster2.getRawData().size());
    }
    
    @Test
    public void testComplexBucketSort3() {
        Map<String, SubCluster<String>> buckets = new HashMap<>();
        SubCluster<String> cluster1 = new SubCluster<>();
        SubCluster<String> cluster2 = new SubCluster<>();
        buckets.put("Male", cluster1);
        buckets.put("Female", cluster2);
        Cluster instance = new Cluster(Collections.emptyList(), 2, 3);

        instance.bucketSort("Transgender", buckets);
        instance.bucketSort("Unknown", buckets);
        instance.bucketSort("Not provided", buckets);
        assertEquals(1, cluster1.getRawData().size());
        assertEquals(2, cluster2.getRawData().size());
    }
}
