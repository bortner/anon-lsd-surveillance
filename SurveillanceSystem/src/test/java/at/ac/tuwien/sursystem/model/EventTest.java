/**
  Copyright (C) 2015 Bernhard Ortner (e0727931@student.tuwien.ac.at)
  License : CC-BY-SA 3.0
  	This file is under the licence Attribution-ShareAlike 3.0 Unported 
	(CC BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/

  You are free :
  	to Share — to copy, distribute and transmit the work
	to Remix — to adapt the work

  Under the following conditions :
  	Attribution. You must attribute the work in the manner
		specified by the author or licensor (but not
		in any way that suggests that they endorse you
		or your use of the work).
	Share Alike. If you alter, transform, or build upon this
		work, you may distribute the resulting work only
		under the same, similar or a compatible license.

 With the understanding that:

 Waiver - Any of the above conditions can be waived if you get permission 
 from the copyright holder.

 Public Domain - Where the work or any of its elements is in the public 
 domain under applicable law, that status is in no way affected by the 
 license.
 
 Other Rights - In no way are any of the following rights affected by the 
 license:
 Your fair dealing or fair use rights, or other applicable copyright 
 exceptions and limitations;
 
 The author's moral rights;
  Rights other persons may have either in the work itself or in how the work 
  is used, such as publicity or privacy rights.
**/
package at.ac.tuwien.sursystem.model;

/**
 *
 * @author Bernhard
 */
/*
 * Copyright 2015 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import at.ac.tuwien.engine.util.events.AnonEvent;
import at.ac.tuwien.engine.util.DatapointListener;
import com.google.common.eventbus.EventBus;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventListener;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bernhard
 */
public class EventTest {

    private transient final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public EventTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void PubSubTest() {
        
        // given
    EventBus eventBus = new EventBus("test");
    DatapointListener listener = new DatapointListener();

    eventBus.register(listener);

    // when
    Datapoint dp = new Datapoint();
    dp.setWhen(new Date(100));
    Datapoint dp2 = new Datapoint();
    dp.setWhen(new Date(101));
    eventBus.post(new AnonEvent(dp));
    eventBus.post(new AnonEvent(dp2));

    // then
    assertEquals(dp2.getWhen(), listener.getLastMessage().getWhen());
    assertNotSame(dp.getWhen(), listener.getLastMessage().getWhen());
    assertEquals(2, listener.getQueuedDPs().size());
    }
}

